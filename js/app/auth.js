function initDateOfBirth()
{
    var d = '';
    var m = '';
    var y = '';
    if (USER_IS_LOGGED_IN) {
        d = moment(getValueFromSession('user_born_at'), MOMENT_FORMAT_SHORT).format('DD');
        m = moment(getValueFromSession('user_born_at'), MOMENT_FORMAT_SHORT).format('MM');
        y = moment(getValueFromSession('user_born_at'), MOMENT_FORMAT_SHORT).format('YYYY');
    }

    // days
    var html = '<option>Day</option>';
    for (var i = 1; i < 32; i++) {
        var s = '';
        var val = '' + (i < 10 ? '0' + i : i);
        if (USER_IS_LOGGED_IN && val == d) {
            s = 'selected';
        }
        html += '<option ' + s + '>' + val + '</option>';
    }
    $('.birth-day').html(html);

    // months
    var months = [
        {'value': '01', 'label': 'January'},
        {'value': '02', 'label': 'February'},
        {'value': '03', 'label': 'March'},
        {'value': '04', 'label': 'April'},
        {'value': '05', 'label': 'May'},
        {'value': '06', 'label': 'June'},
        {'value': '07', 'label': 'July'},
        {'value': '08', 'label': 'August'},
        {'value': '09', 'label': 'September'},
        {'value': '10', 'label': 'October'},
        {'value': '11', 'label': 'November'},
        {'value': '12', 'label': 'December'}
    ];
    var html = '<option>Month</option>';
    for (var i = 0; i < months.length; i++) {
        var s = '';
        var val = months[i]['value'];
        if (USER_IS_LOGGED_IN && val == m) {
            s = 'selected';
        }
        html += '<option ' + s + ' value="' + val + '">' + months[i]['label'] + '</option>';
    }
    $('.birth-month').html(html);

    // years
    var html = '<option>Year</option>';
    var fromD = moment().subtract(70, 'years').format('YYYY');
    var toD = moment().subtract(12, 'years').format('YYYY');
    for (var i = toD; i >= fromD; i--) {
        var s = '';
        if (USER_IS_LOGGED_IN && i == y) {
            s = 'selected';
        }
        html += '<option ' + s + '>' + i + '</option>';
    }
    $('.birth-year').html(html);
}

function registerAuth()
{
    $('#' + PAGE_REGISTER + ' .birth-day, #' + PAGE_REGISTER + ' .birth-month, #' + PAGE_REGISTER + ' .birth-year').change(function ()
    {
        updateDateOfBirth(PAGE_REGISTER);
    });

    // MALE or FEMALE Checks
    $('.gender:button').off('tap');
    $('.gender:button').on('tap', function (e)
    {
        killEvent(e);
        var this_link = $(this);
        var all_links = $('.gender:button');
        var other_links = all_links.not(this_link);

        if (!this_link.hasClass('active')) {
            other_links.removeClass('active');
            this_link.addClass('active');
        }
        return false;
    });

    // skip login
    $('.page-skip-link').on('tap', function (e)
    {
        killEvent(e);
        disableEnableButton($(this));

        USER_SKIPPED = true;
        goToPage(PAGE_DASHBOARD);

        INTERESTS.getInterests(true);
        return false;
    })

    // login
    $('#btn-login-submit').off('tap');
    $('#btn-login-submit').on('tap', function (e)
    {
        var btn = $(this);
        stopSubmitButton($(this), e);

        var valid = '';
        valid += validateInput(input(PAGE_LOGIN, 'in-email'));
        valid += validateInput(input(PAGE_LOGIN, 'in-password'));

        if (valid.length > 1) {
            showButtonAlertError($(this));
            return false;
        } else {
            if (!validateEmail(input(PAGE_LOGIN, 'in-email'))) {
                showButtonAlertError($(this), 'Please enter a valid email.');
                return false;
            }
        }

        var o = getDeviceInformation();
        o['username'] = inputVal(PAGE_LOGIN, 'in-email');
        o['password'] = inputVal(PAGE_LOGIN, 'in-password');
        doServerCall(URL_LOGIN, o, function (response)
        {
            enableButton(btn);

            if (validateResponse(response, btn)) {
                updateUser(response.user);

                goToPage(PAGE_DASHBOARD);
            }
        });

        return false;
    });

    // register
    $('#btn-register-submit').off('tap');
    $('#btn-register-submit').on('tap', function (e)
    {
        var btn = $(this);
        stopSubmitButton($(this), e);

        var valid = "";
        valid += validateInput(input(PAGE_REGISTER, 'in-firstname'));
        valid += validateInput(input(PAGE_REGISTER, 'in-lastname'));
        valid += validateInput(input(PAGE_REGISTER, 'in-email'));

        // validate inputs
        if (valid.length > 1) {
            showButtonAlertError(btn, 'Please complete the form.');
            return;
        }

        // validate email
        if (!validateEmail(input(PAGE_REGISTER, 'in-email'))) {
            showButtonAlertError(btn, 'Please enter a valid email.');
            return false;
        }

        // validate password and confirm password
        var valid = "";
        valid += validateInput(input(PAGE_REGISTER, 'in-password'), 4);
        valid += validateInput(input(PAGE_REGISTER, 'in-password-confirm'), 4);

        // password fields min 4 length
        if (valid.length > 1) {
            showButtonAlertError(btn, 'Please enter your password and confirm password.');
            return false;
        }

        // password fields same values
        var pass = inputVal(PAGE_REGISTER, 'in-password');
        var passConfirm = inputVal(PAGE_REGISTER, 'in-password-confirm');

        if (pass != passConfirm) {
            showButtonAlertError(btn, 'The passwords do not match.');
            return false;
        }

        var o = getDeviceInformation();
        o['firstname'] = inputVal(PAGE_REGISTER, 'in-firstname');
        o['lastname'] = inputVal(PAGE_REGISTER, 'in-lastname');
        o['city'] = inputVal(PAGE_REGISTER, 'in-city');
        o['cellphone'] = inputVal(PAGE_REGISTER, 'in-cellphone');
        o['email'] = inputVal(PAGE_REGISTER, 'in-email');
        o['password'] = inputVal(PAGE_REGISTER, 'in-password');

        var gender = 'profile';
        if ($('#' + PAGE_REGISTER + ' .gender:button.male').hasClass('active')) {
            gender = 'male';
        }
        if ($('#' + PAGE_REGISTER + ' .gender:button.female').hasClass('active')) {
            gender = 'female';
        }

        o['gender'] = gender;
        o['born_at'] = inputVal(PAGE_REGISTER, 'in-born');

        if (!moment(o['born_at'], MOMENT_FORMAT_SHORT).isValid()) {
            o['born_at'] = null;
        }

        doServerCall(URL_REGISTER, o, function (response)
        {
            enableButton(btn);

            if (response.success && response.success == true) {
                var username = inputVal(PAGE_REGISTER, 'in-firstname');
                showButtonAlertSuccess(btn, 'Welcome ' + username + ', we will redirect you to the login page.');
                goToPageDelay(PAGE_LOGIN, 3000);
                return false;
            }

            if (validateResponse(response, btn)) {
                updateUser(response.user);

                PAGE_QUEUE = ['page-dashboard'];

                //goToPage(PAGE_DASHBOARD);
                goToPage(PAGE_DASHBOARD);
            }
        });

        return false;
    });

    // login
    $('#btn-forgot-password-submit').off('tap');
    $('#btn-forgot-password-submit').on('tap', function (e)
    {
        var btn = $(this);
        stopSubmitButton($(this), e);

        var valid = '';
        valid += validateInput(input(PAGE_FORGOT_PASSWORD, 'in-email'));

        if (valid.length > 1) {
            showButtonAlertError($(this), 'Please enter your email.');
            return false;
        } else {
            if (!validateEmail(input(PAGE_FORGOT_PASSWORD, 'in-email'))) {
                showButtonAlertError($(this), 'Please enter a valid email.');
                return false;
            }
        }

        var o = getDeviceInformation();
        o['email'] = inputVal(PAGE_FORGOT_PASSWORD, 'in-email');
        doServerCall(URL_FORGOT_PASSWORD, o, function (response)
        {
            enableButton($(this));

            if (validateResponse(response, btn)) {
                inputReset(PAGE_LOGIN, 'in-email');
                showButtonAlertSuccess(btn, response.data.title, response.data.content, '');
            }
        });

        return false;
    });
}

/**
 * Set the hidden born at input field - from date dropdowns
 */
function updateDateOfBirth(page)
{
    var birthDay = selectText(page, 'birth-day');
    var birthMonth = selectVal(page, 'birth-month');
    var birthYear = selectText(page, 'birth-year');

    input(page, 'in-born').val(birthYear + '-' + birthMonth + '-' + birthDay);
};

function resetLoginForm()
{
    inputReset(PAGE_LOGIN, 'in-email');
    inputReset(PAGE_LOGIN, 'in-password');

    $("#feedback-login").css('display', "none");
}

function resetForgotPasswordForm()
{
    inputReset(PAGE_FORGOT_PASSWORD, 'in-email');

    $("#feedback-forgot-password").css('display', "none");
}

/**
 * Reset the registration form
 */
function resetRegisterForm()
{
    input(PAGE_REGISTER, 'in-firstname').val('');
    input(PAGE_REGISTER, 'in-lastname').val('');
    input(PAGE_REGISTER, 'in-born').val('');
    input(PAGE_REGISTER, 'in-token').val('');

    $("#feedback-register").css('display', "none");
    $('#page-register .gender button.male').removeClass('active');
    $('#page-register .gender button.female').removeClass('active');
}