var WalkthroughClass = function ()
{
    var root = this;

    this.initAndShow = function ()
    {
        $('#walkthrough').fadeIn(400);

        $('#walkthrough .slick-container').slick({
            speed: 350,
            dots: false,
            swipe: true,
            arrows: false,
            slidesToShow: 1,
            infinite: true,
            draggable: true,
            touchMove: true,
            centerMode: true,
            slidesToScroll: 1,
            focusOnSelect: true,
            centerPadding: '0px'
        });
    }

    this.hideAndDestroy = function() {
        
    }

    this.getAdverts = function (showLoader)
    {
        showLoader = showLoader ? showLoader : false;
        doServerCall(URL_ADVERTS, null, function (response)
        {
            if (response['success'] != true) {
                // showNotification('Whoops, we could not get the adverts', 'error');
                return false;
            }

            root.adverts = response['adverts'];

            // if we wanted to add an advert but server not received yet
            if (root.container && root.adverts.length > 0) {
                root.renderArticleAdvert(root.container);
            }
        }, showLoader, 'GET');
    };

    var updateArticleTemplate = function (html, data)
    {
        var height = $(window).height() - 100;
        var width = $(window).width() > 700 ? 700 : $(window).width();

        html.attr('data-id', data['advert_id']);
        html.attr('id', 'advert-list-' + data['advert_id']);
        html.find('.div-image').css('background-image', 'url(' + IMAGES_PATH + data['img_file'] + ')');
        html.find('.div-image').css({'width': width + 'px', 'height': height + 'px'});
        //html.find('.div-image').attr('data-original', ARTICLE_IMAGES_PATH + data['image']);
        //html.find('.img-responsive').attr('data-original', ARTICLE_IMAGES_PATH + data['image']);
        return html;
    }

    var registerEvents = function ()
    {

        $('.article-addvvertt').off('tap');
        $('.article-addvvertt').on('click', function (e)
        {
            killEvent(e);
            disableEnableButton($(this));

            var advert = findItemInArray($(this).attr('data-id'), root.adverts, 'advert_id');
            if (advert['url'] && advert['url'].length > 5) {
                openQRUrlInBrowser(advert['url']);
            }
        });
    }
};