// cached pages
var ARTICLE_PAGES = [];
var SELECTED_ARTICLE_PAGE = 0;

var ARTICLE_TYPES = {
    'publication': {
        'key': 'pub_id',
        'ids_key': 'pub_id',
        'url': URL_ARTICLES_PUBLICATION,
    },
    'category': {
        'key': 'cat_id',
        'ids_key': 'category_ids',
        'url': URL_ARTICLES_CATEGORIES,
    }
};

var ArticlesPage = function (options)
{
    var vars = {
        ids: '0',
        type: 'dashboard',
        idsKey: 'cat_id',
        cacheKey: 'dashboard',
        url: URL_ARTICLES_CATEGORIES,
        cacheKeyPrefix: 'articles_items_',
        container: $('#page-dashboard .article-list-box')
    };

    var serverObject = {
        'limit': 0,
        'offset': 0,
    };

    var root = this;
    root.key = '';
    root.limit = 0; // number of items to show
    root.offset = 0; // total items already rendered
    root.articles = [];
    root.limitReached = false;
    root.selectedArticle = [];
    root.busyFetchingData = false;

    root.scrollPosition = 0; // when we select a article

    /*
     * Constructor
     */
    this.construct = function (options)
    {
        $.extend(vars, options);

        resetClass(false);
    };

    /**
     * Only for dashboard - update the ids
     */
    this.updateIds = function ()
    {
        if (vars.cacheKey == 'dashboard') {
            vars.ids = INTERESTS.getCategoriesIdsList();
            serverObject[vars.idsKey] = vars.ids;
        }
    }

    /**
     * Main function to get and show the items
     * if we reset or force - clear and reset everything
     * If we have cached data - show them
     * If no cached - get new items from server
     * @param reset
     * @param force
     */
    this.getAndShowItems = function (reset, force)
    {
        // busy getting data from server
        if (root.busyFetchingData) {
            return false;
        }

        // if we reset or force - remove limit
        if (reset == true || force == true) {
            root.limitReached = false;
        }

        // limit reached
        if (root.limitReached) {
            hideLoader(true);
            return false;
        }

        busyFetchingFromServer(true);

        // check if we need to reset
        if (reset == true || force == true) {
            resetClass(force);
            busyFetchingFromServer(true);
        } else {
            root.offset += root.limit;
        }

        getItems();
    }

    /**
     * Get the key used to save data locally
     * @returns {string}
     */
    var getUniqueKey = function ()
    {
        return vars.cacheKeyPrefix + vars.cacheKey;
    }

    /**
     * Clear all vars - delete cache
     */
    var resetClass = function (force)
    {
        // root vars
        root.key = getUniqueKey();
        root.articles = [];
        root.limitReached = false;
        root.busyFetchingData = false;
        root.selectedArticle = [];
        root.limit = 9; // number of items to show
        root.offset = 0; // total items already rendered

        // if not specified - get from all categories
        if (vars.ids == '0') {
            vars.ids = INTERESTS.getCategoriesIdsList();
        }

        // server object
        serverObject.limit = 54; // 60 // 12|limit * 5|pages
        serverObject.offset = 0;
        serverObject[vars.idsKey] = vars.ids;

        // delete cached data if force
        if (force == true) {
            deleteCacheData(root.key);
            deleteCacheData(root.key + LOCAL_DATA_SUFFIX);
        }

        // set the cached items to the current articles list
        if (getValueFromSession(root.key)) {
            root.articles = JSON.parse(getValueFromSession(root.key));
        }
    }

    /**
     * Check if items in cache
     * @returns {boolean}
     */
    var getItems = function ()
    {
        // check the items in cache
        // if false - get from server
        if (!renderCachedArticles()) {

            // set server's offset same as locals offset
            serverObject.offset = root.offset;
            getArticlesNMH(vars.url, serverObject,
                function (response)
                {
                    // update timestamp
                    cacheDataTimestamp(root.key);

                    // we have reached the limit
                    if (response.length <= 1) {
                        showLimitReached();
                        return false;
                    }

                    // append newly items to current cache
                    var finalItems = root.articles;
                    for (var i = 0; i < response.length; i++) {
                        finalItems.push(response[i]);
                    }

                    root.articles = finalItems;
                    cacheData(root.key, JSON.stringify(finalItems));

                    renderArticles();
                }
            );
        }
    }

    /**
     * Render cached articles
     * @param key
     */
    var renderCachedArticles = function (key)
    {
        if (checkCachedData(root.key, 12)) {
            // check if we have the next items in cache
            if (root.articles.length > root.offset) {
                renderArticles();
                return true;
            }
        }

        return false;
    }

    /**
     * Renders the articles
     * @param items
     */
    var renderArticles = function ()
    {
        // get the items from the cache
        var items = root.articles;
        var total = items.length;

        // there are more items, get the limit
        // if less items, we loop till the end
        if (total >= (root.offset + root.limit)) {
            total = (root.offset + root.limit);
        }

        // unregister
        vars.container.find('.readmore').off('tap');

        // only clear on first time
        if (root.offset == 0) {
            vars.container.html('');
        }

        // loop through all items we need to render (page's items)
        for (var i = root.offset; i < total; i++) {
            var data = items[i];

            if (i % 3 == 0 || i == 0) {
                var cloned = $('#template-article').clone();
                vars.container.append(updateArticleTemplate(cloned, data));
            } else {
                //var small = (i % 3 == 1 || i % 3 == 2)? true:false;
                // skip first small, it generate both
                if (i % 3 == 2) {
                    var html = showListLayoutSmallRow(items[i - 1], items[i]);
                    vars.container.append(html);
                }
            }
        }

        registerArticleReadMore();

        hideLoader(true);
        busyFetchingFromServer(false);

        ADVERTS.renderArticleAdvert(vars.container);
    }

    /**
     * Register the tap events
     */
    var registerArticleReadMore = function ()
    {
        vars.container.find('.readmore').off('tap');
        vars.container.find('.readmore').on('tap', function (e)
        {
            DASHBOARD.selectedArticle = findItemInArray($(this).attr('data-id'), root.articles, 'post_id');
            if (DASHBOARD.selectedArticle) {
                DASHBOARD.scrollPosition = $('body').scrollTop();
                showArticle();
                goToPage(PAGE_SHOW_ARTICLE);
            }
        });
    }

    /**
     * Flag if we are still getting content from the db
     * As the scroller can call multiple times
     * @param value
     */
    var busyFetchingFromServer = function (value)
    {
        // delay if we activate agaion
        if (value == false) {
            setTimeout(function ()
            {
                root.busyFetchingData = false;
            }, 1000);
        } else {
            root.busyFetchingData = true;
        }
    }

    /**
     * Update the article template html with data
     *
     * @param html
     * @param data
     * @returns {*}
     */
    var updateArticleTemplate = function (html, data)
    {
        html.attr('id', 'article-list-' + data['post_id']);

        // format date
        var momentt = moment(data['datetime'], MOMENT_FORMAT_FULL);
        var overviewDate = momentt.format('D MMM YYYY');
        var humanize = momentt.fromNow();

        var brandImage = slugImageName(data['publication'].toLowerCase());

        html.find('.heading').html(data['title']);
        html.find('.brand').html(data['publication']);
        html.find('.author').html(data['author']);
        html.find('.subtitle').html(data['heading']);
        html.find('.content-story').html(data['body']);
        html.find('.readmore').attr('data-id', data['post_id']);
        html.find('.category').html(data['cat_name']);

        html.find('.date').html(overviewDate + ' &middot; ' + humanize);
        html.find('.div-image').css('background-image', 'url(' + ARTICLE_IMAGES_PATH + data['image'] + ')');
        //html.find('.div-image').attr('data-original', ARTICLE_IMAGES_PATH + data['image']);
        //html.find('.img-responsive').attr('data-original', ARTICLE_IMAGES_PATH + data['image']);
        html.find('.publication-logo img').attr('src', 'images/publications/' + brandImage + '_small.png');

        // show / hide the footer icons
        $('#footer-article .category').html(data['cat_name']);
        $('#footer-article .article-map').hide();
        if (data['location'] && data['location'].length > 2) {
            $('#footer-article .article-map').show();
            $('#footer-article .article-map').attr('data-href', GOOGLE_MAPS_QUERY + encodeURI(data['location']));
        }

        $('#footer-article .article-link').hide();
        $('#footer-article .article-link').attr('data-href', '');

        var fbshares = data['fb_share'] + checkPluralWord(' share', parseInt(data['fb_share']));
        var nas = data['total_na'] + ' nja!';
        $('#footer-article .article-comments').html(data['fb_comments'] + checkPluralWord(' Comment'), data['fb_comments']);
        $('#footer-article .article-social').html(fbshares + '  &middot; ' + nas);

        $('#footer-article .btn-share[data-type="facebook_share"]').attr('data-id', data['post_id']);
        $('#footer-article .btn-share[data-type="twitter_share"]').attr('data-id', data['post_id']);
        $('#footer-article .btn-share[data-type="whatsapp"]').attr('data-id', data['post_id']);
        $('#footer-article .btn-share[data-type="nja"]').attr('data-id', data['post_id']);

        $('#footer-article .btn-share[data-type="facebook_share"]').find('span').html(data['fb_share']);
        $('#footer-article .btn-share[data-type="twitter_share"]').find('span').html(data['twitter_mentions']);
        $('#footer-article .btn-share[data-type="nja"]').find('span').html(data['total_na']);

        return html;
    }

    /**
     * Generate the article small row
     * @param data1
     * @param data2
     * @returns {string}
     */
    var showListLayoutSmallRow = function (data1, data2)
    {
        var row = '<div class="article-row">';
        row += showListLayoutSmall(data1);
        if (data2) {
            row += showListLayoutSmall(data2);
        }

        return row + '</div>';
    }

    /**
     * Show article list latout small
     * @param data
     * @returns {string}
     */
    var showListLayoutSmall = function (data)
    {
        var cloned = $('#template-article-small').clone();
        var html = updateArticleTemplate(cloned, data);
        var prefix = '<div class="col-6"><div id="article-list-';
        return prefix + data['post_id'] + '"' + ' class="article small">' + html.html() + '</div></div>';
    }

    /**
     * public function - show article
     */
    var showArticle = function ()
    {
        updateArticleTemplate($('#page-show-article .article-show'), DASHBOARD.selectedArticle);
        $('#page-show-article .article-show').attr('id', 'article-show-' + DASHBOARD.selectedArticle['post_id']);

        registerShareLinks();

        //COMMENTS.registerEvents();
        COMMENTS.selectedArticle = DASHBOARD.selectedArticle;
    }

    var showLimitReached = function ()
    {
        root.limitReached = true;

        var html = '<div style="padding: 15px; margin-bottom:20px;"><h1>You have reached the end, there are no more articles</h1></div>';
        vars.container.append(html);

        hideLoader(true);
        busyFetchingFromServer(false);
    }

    this.tmpSelectedArticle = function ()
    {

        DASHBOARD.selectedArticle = findItemInArray('948', root.articles, 'post_id');
        if (DASHBOARD.selectedArticle) {
            showArticle();
            goToPage(PAGE_SHOW_ARTICLE);
        }
    }

    this.construct(options);
};