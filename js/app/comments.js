var CommentsClass = function (options)
{
    var vars = {
        myVar: 'original Value'
    };

    var root = this;

    root.selectedArticle = [];

    /*
     * Constructor
     */
    this.construct = function (options)
    {
        $.extend(vars, options);
    };

    this.registerEvents = function ()
    {
        $('.link-article-comments').off('tap');
        $('.link-article-comments').on('tap', function (e)
        {
            e.preventDefault();
            disableEnableButton($(this));

            getAndShowComments(e);
            goToPage(PAGE_ARTICLE_COMMENTS);
        });

        $('#btn-comments-submit').off('tap');
        $('#btn-comments-submit').on('tap', function (e)
        {
            var btn = $(this);
            stopSubmitButton(btn, e);

            var comment = inputVal(PAGE_ARTICLE_COMMENTS, 'in-comment');
            if (comment.length < 3) {
                showButtonAlertError($(this), 'Please enter your comment');
                return false;
            }

            doServerCall(URL_ARTICLE_COMMENTS_CREATE, {
                'comment': comment,
                'post_id': root.selectedArticle['post_id']
            }, function (response)
            {
                enableButton(btn);
                getAndShowComments();
                showNotification('Thank you for the comment');

                inputReset(PAGE_ARTICLE_COMMENTS, 'in-comment');
            });
        });
    }

    var getAndShowComments = function ()
    {
        doServerCall(URL_ARTICLE_COMMENTS, {
            'id': root.selectedArticle['post_id'],
            'cat_id': 2
        }, showComments);
    };

    var showComments = function (response)
    {
        $('#' + PAGE_ARTICLE_COMMENTS + ' .comments-title').html(root.selectedArticle['title']);
        $('#comments-container').html('');

        // render all comments
        for (var i = 0; i < response.length; i++) {

            //var momentt = moment(response[i]['datetime'], MOMENT_FORMAT_FULL);
            //var overviewDate = momentt.format('D MMM YYYY');
            //var humanize = momentt.fromNow();

            var html = '<div class="media comment text-gray-dark"><div class="media-body"><h4 class="media-heading">';
            html += response[i]['author'];
            html += '<span class="font-80 pull-right">5 hours ago</span>';
            html += '</h4>';
            html += response[i]['title'] + '</div></div>';
            $('#comments-container').append(html);
        }
    };

    /*
     * Pass options when class instantiated
     */
    this.construct(options);
};