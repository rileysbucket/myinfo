var MyPassportClass = function ()
{
    var root = this;

    root.loyalties = [];

    this.getUserPassInfo = function ()
    {
        doServerCall(URL_USER_PASS, null, function (response)
        {
            if (response['success']) {
                $('.user-qrcode-token').html(response['code']);
                $('.user-qrcode-pass').attr('src', response['qr_code_file']);
            }
        }, false);
    }

    this.getLoyalties = function (showLoader)
    {
        showLoader = showLoader ? showLoader : false;
        doServerCall(URL_LOYALTIES, null, function (response)
        {
            if (response['success'] != true) {
                showNotification('Whoops, we could not get the loyalty points', 'error');
                return false;
            }

            root.loyalties = [];
            for (var i = 0; i < response['promotions'].length; i++) {
                if (response['promotions'][i]['TYPE'] == 'touch_n_win') {
                    root.loyalties.push(response['promotions'][i]);
                }
            }

            renderLoyalties();
        }, showLoader);
    };

    /**
     * Render all the vouchers to the screen
     */
    var renderLoyalties = function ()
    {
        $('#loyalties-container').html('');
        for (var i = 0; i < root.loyalties.length; i++) {
            var cloned = $('#template-loyalties-badges').clone();
            var html = updateTemplate($(cloned.html()), root.loyalties[i]);

            $('#loyalties-container').append(html);
        }

        $('.badge-rating').rating();
    };

    /**
     * Set the information in the tempalte
     * @param html
     * @param data
     * @returns {*}
     */
    var updateTemplate = function (html, data)
    {
        var badges = parseInt(data['badges']);
        var badgesMax = parseInt(data['MAX_BADGES']);
        if (badges > 10) {
            badges = 10;
        }

        html.attr('data-id', data['ID']);
        html.find('.badge-title').html(data['NAME']);
        html.find('.badge-rating-template').attr('data-stop', badgesMax);
        html.find('.badge-rating-template').attr('value', badges);
        html.find('.badge-rating-template').addClass('badge-rating');

        return html;
    };
};