var InterestsClass = function ()
{
    var root = this;

    root.myna = [
        'Property',
        'Cars',
        'Auction',
        'Buy & Sell',
        'Deals',
        'Business',
        'Jobs',
        'Tenders',
        'Events'
    ];
    root.allInterests = [];
    root.publications = [];
    root.categories = [];

    root.userPublications = [];
    root.userCategories = [];

    this.init = function ()
    {
        root.getInterests();
    }

    this.getInterests = function (force)
    {
        var key = 'interests1';
        // check cache
        if (force != true && checkCachedData(key, 120)) {
            renderCachedInterests(key);
        } else { // get new from server
            showLoader(true);
            doServerCall(URL_INTERESTS, null, function (response)
            {
                if (response.success && response.success == true) {
                    cacheDataTimestamp(key);
                    cacheData(key, JSON.stringify(response));

                    root.showMyIniterests(response.publications, response.categories, force);
                }
            });
        }
    }

    this.showMyIniterests = function (publications, categories, force)
    {
        log('this.showMyIniterests -- ' + publications.length + ' -- categories -- ' + categories.length + ' force?? ' + force);
        //log(publications);
        //log(categories);

        root.publications = publications;
        root.categories = categories;
        root.allInterests = publications.concat(categories);

        root.userPublications = [];
        root.userCategories = [];
        var total = 0;
        var items = root.publications;
        $('#page-my-interests .brand-items').html('');
        for (var i = 0; i < items.length; i++) {
            var o = items[i];
            if (o['selected']) {
                total++;
                var html = '<div class="iterests-item media deeplink-interest" data-id="publication-' + o['pub_id'] + '">';
                html += '<div class="publication-logo media-left"><img src="images/publications/' + slugImageName(o['title']) + '_small.png"/></div>';
                html += '<div class="media-body"><p class="heading text-wrap">' + o['title'] + '</p>';
                if (o['body'] && o['body'].length > 2) {
                    html += '<p class="summary text-wrap">' + o['body'] + '</p>';
                }
                html += '</div><div class="media-right media-middle">';
                html += '<i class="icon-readmore text-gray-light"></i></div></div>';

                root.userPublications.push(o);
                $('#page-my-interests .brand-items').append(html);
            }
        }

        if (total <= 0) {
            html = '<div class="iterests-item media">You do not have any publications</div>';
            $('#page-my-interests .brand-items').append(html);
        }

        $('#page-my-interests .myna-interests').html('');
        for (var i = 0; i < root.myna.length; i++) {
            var html = '<div class="iterests-item media">';
            html += '<div class="media-body"><p class="heading text-wrap">' + root.myna[i] + '</p></div>';
            html += '<div class="media-right media-middle">';
            html += '<i class="icon-readmore text-gray-light"></i></div></div>';

            $('#page-my-interests .myna-interests').append(html);
        }

        total = 0;
        var items = root.categories;
        $('#page-my-interests .category-items').html('');
        for (var i = 0; i < items.length; i++) {
            var o = items[i];
            if (o['selected']) {
                total++;
                var html = '<div class="iterests-item media deeplink-interest" data-id="category-' + o['cat_id'] + '">';
                html += '<div class="media-body"><p class="heading text-wrap">' + o['title_group'] + '</p></div>';
                html += '<div class="media-right media-middle">';
                html += '<i class="icon-readmore text-gray-light"></i></div></div>';

                o['title'] = o['title_group'];
                root.userCategories.push(o);
                $('#page-my-interests .category-items').append(html);
            }
        }

        if (total <= 0) {
            html = '<div class="iterests-item media">You do not have any categories</div>';
            $('#page-my-interests .category-items').append(html);
        }

        root.showEditIniterests();
        registerInterestPageLinks();

        DASHBOARD.updateIds();
        DASHBOARD.getAndShowItems(true, force);
        HEADER.updateUserArticles();

        checkNoInterestsUser();
    }

    this.showEditIniterests = function ()
    {
        var publications = root.publications;
        $('#page-edit-interests .brand-items').html('');
        for (var i = 0; i < publications.length; i++) {
            var o = publications[i];
            var html = '<div data-id="' + o['pub_id'] + '" class="iterests-item media toggle-interest">';
            html += '<div class="publication-logo media-left"><img src="images/publications/' + slugImageName(o['title']) + '_small.png"/></div>';
            html += '<div class="media-body"><p class="heading text-wrap">' + o['title'] + '</p>';
            if (o['body'] && o['body'].length > 2) {
                html += '<p class="summary text-wrap">' + o['body'] + '</p>';
            }
            html += '</div><div class="media-right media-middle">';
            html += '<input data-id="' + o['pub_id'] + '" type="checkbox" class="checkbox" ' + (o['selected'] ? 'checked' : '') + '/></div></div>';

            $('#page-edit-interests .brand-items').append(html);
        }

        var items = root.categories;
        $('#page-edit-interests .category-items').html('');
        for (var i = 0; i < items.length; i++) {
            var o = items[i];
            var html = '<div data-id="' + o['cat_id'] + '" class="iterests-item media toggle-interest">';
            html += '<div class="media-body"><p class="heading text-wrap">' + o['title_group'] + '</p></div>';
            html += '<div class="media-right media-middle">';
            html += '<input data-id="' + o['cat_id'] + '" type="checkbox" class="checkbox" ' + (o['selected'] ? 'checked' : '') + '/></div></div>';

            $('#page-edit-interests .category-items').append(html);
        }

        registerInterestPageLinks();
    }

    this.updateMyInterests = function ()
    {
        var publications = getCheckboxIds($('#page-edit-interests .brand-items .checkbox'));
        var categories = getCheckboxIds($('#page-edit-interests .category-items .checkbox'));

        showLoader(true);
        doServerCall(URL_UPDATE_INTERESTS, {
            'publications': publications,
            'categories': categories
        }, function (response)
        {
            showNotification("Interests successfully updated");
            root.getInterests(true);

            // if logged in
            if (USER_IS_LOGGED_IN) {
                forcePageRedirect(PAGE_DASHBOARD);
                HEADER.toggleMenuBar('articles');
            }
        });
    }

    this.getCategoriesIdsList = function ()
    {
        var ids = createListFromArray(root.userCategories, 'category_ids');
        if (root.userCategories.length <= 0) {
            ids = createListFromArray(root.categories, 'category_ids');
        }

        return ids;
    }

    var renderCachedInterests = function (key)
    {
        var data = getValueFromSession(key);
        if (data) {
            var items = JSON.parse(data);

            root.showMyIniterests(items.publications, items.categories, false);
        } else { // data not in cache
            // check internet
            if (isNetworkConnected()) {
                root.init();
            } else {
                showPopup("Network", "Please connect to the internet to get the latest interests.");
            }
        }
    }

    var registerInterestPageLinks = function ()
    {
        $('.deeplink-interest').off('tap');
        $('.deeplink-interest').on('tap', function (e)
        {
            killEvent(e);
            disableEnableButton($(this));

            var id = $(this).attr('data-id');

            var list = $('#menu-articles .slick-container .slick-slide');
            var index = 0;
            list.each(function ()
            {
                var slide = $(this);
                var link = $(this).find('a');
                // there are a couple of 'cloned' items
                if (!slide.hasClass('slick-cloned')) {
                    if (link.attr('data-page-id') == id) {
                        HEADER.toggleMenuBar('articles', index);
                    }

                    index++;
                }
            });
            return false;
        });

        $('.toggle-interest').off('tap');
        $('.toggle-interest').on('tap', function (e)
        {
            killEvent(e);
            disableEnableButton($(this));

            var check = $(this).find('.checkbox');
            check.prop("checked", !check.prop("checked"));
            return false;
        });
    }
};