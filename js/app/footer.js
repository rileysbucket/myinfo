var FooterClass = function (options)
{

    var vars = {};

    var root = this;

    this.construct = function (options)
    {
        $.extend(vars, options);

        registerEvents();
    };

    this.toggle = function (type)
    {
        type = type ? type : 'default';

        // if already visible / selected
        if ($('#footer-' + type).hasClass('active')) {
            return false;
        }

        var from = $('#footer .footer-box.active');
        var to = $('#footer-' + type);

        from.removeClass('active').addClass('moveBottom');
        to.removeClass('moveBottom').addClass('active');
    }

    var registerEvents = function ()
    {
        $('.footer-logo').off('tap');
        $('.footer-logo').on('tap', function (e)
        {
            killEvent(e);
            disableEnableButton($(this));

            DASHBOARD.getAndShowItems(true, true);
            HEADER.toggleMenuBar('articles', 0);
            forcePageRedirect(PAGE_DASHBOARD);
        })

        $('#btn-footer-submit').off('tap');
        $('#btn-footer-submit').on('tap', function (e)
        {
            killEvent(e);
            disableEnableButton($(this));

            if (!checkAuthGuard('Unfortunately you have to be signed in to save your interests.')) {
                return false;
            }

            var type = $(this).attr('data-type');
            if (type == 'edit-interests') {
                INTERESTS.updateMyInterests();
            }
        });
    };

    this.construct(options);
};