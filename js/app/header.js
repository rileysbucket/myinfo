var HeaderClass = function (options)
{
    var vars = {
        no_user: 'no-user',
        interests: 'interests',
        articles: 'articles',
    };

    var root = this;

    root.menubar = '';
    root.headerScroll = '';

    root.articlesBox = $('#menu-articles .slick-container');
    root.articlesBoxString = '#menu-articles .slick-container';

    // construct
    this.construct = function (options)
    {
        $.extend(vars, options);

        registerEvents();
        registerHeaderPageLinks();
    };

    this.init = function ()
    {
        initInterestsMenu();
        //initHeaderHideOnScroll();

        root.toggleMenuBar('articles');
    }

    /**
     * Init the header scroll
     * When scroll, hide the top header part
     */
    this.toggleHeaderScroll = function (type)
    {
        if (PAGE_SELECTED == PAGE_LANDING ||
            PAGE_SELECTED == PAGE_LOGIN ||
            PAGE_SELECTED == PAGE_REGISTER ||
            PAGE_SELECTED == PAGE_FORGOT_PASSWORD) {
            return false;
        }

        if (type == 'hide') {
            $('#header').addClass('scrolled');
        } else {
            $('#header').removeClass('scrolled');
        }

        root.headerScroll = type;
    }

    /**
     * Update menubar with user's articles
     * To show specific categorie's articles
     * @returns {boolean}
     */
    this.updateUserArticles = function ()
    {
        if (!USER_IS_LOGGED_IN) {
            return false;
        }

        // destroy
        destroySlick(root.articlesBoxString);

        // if not active - make active
        if ($('#menu-articles').hasClass('fadeOut')) {
            $('#menu-articles').removeClass('fadeOut').addClass('fadeIn');
        }

        // clear dom
        root.articlesBox.html('');
        var dashboard = '<div><a data-id="dashboard" class="header-page-link btn-small-header">MY INFO</a></div>';
        root.articlesBox.append(dashboard);

        // loop through the categories
        var total = renderArticlesMenu('publication', INTERESTS.userPublications);
        total += renderArticlesMenu('category', INTERESTS.userCategories);

        // render articles menubar
        function renderArticlesMenu(type, items)
        {
            for (var i = 0; i < items.length; i++) {
                var html = $('#template-menu-article').clone();
                var id = items[i][ARTICLE_TYPES[type]['key']];
                html.find('a').html(items[i]['title']);
                html.find('a').attr('data-type', type);
                html.find('a').attr('data-article-id', id);
                html.find('a').attr('data-page-id', type + '-' + id);
                root.articlesBox.append(html.html());
            }

            return items.length;
        }

        // init
        initSlick('articles', root.articlesBoxString, 0, (total > 1));

        // register events after init
        registerPageLinks();
        registerArticlesMenubar();
    }

    this.showPageMenuBar = function (title)
    {
        $('.menu-page-title').html(title.toUpperCase());
        root.toggleMenuBar('page');
    }

    /**
     * Toggle menubar
     * - menu
     * - articles
     * - no user
     * @param type
     */
    this.toggleMenuBar = function (type, slickIndex)
    {
        slickIndex = slickIndex ? slickIndex : 0;

        // fail safe - if not logged in
        if (USER_IS_LOGGED_IN == false && type == 'articles') {
            type = 'no-user';
        }

        // already on page
        if (type == root.menubar) {
            if(type == 'articles') {
                if (root.articlesBox.hasClass('slick-initialized')) {
                    root.articlesBox.slick('slickGoTo', slickIndex);
                }
            }
            return;
        }

        root.menubar = type;

        // if we switch menus - go to a specific page
        switch (type) {
            case 'no-user':
                if (USER_SKIPPED) {
                    forcePageRedirect(PAGE_DASHBOARD);
                }
                break;
            case 'articles':
                if (root.articlesBox.hasClass('slick-initialized')) {
                    root.articlesBox.slick('slickGoTo', slickIndex);
                } else { // open app - deeplink to dashboard
                    forcePageRedirect(PAGE_DASHBOARD);
                }
                break;
            case 'interests':
                forcePageRedirect(PAGE_MY_INTERESTS);
                break;
        }

        var from = $('#menubar .menu-box.active');
        var to = $('#menu-' + type);

        from.removeClass('active').addClass('moveBottom');
        to.removeClass('fadeOut moveTop').addClass('fadeIn active');

        setTimeout(function ()
        {
            from.removeClass('fadeIn moveBottom').addClass('fadeOut moveTop');
            setTimeout(function ()
            {
                from.removeClass('fadeOut');
            }, 400);
        }, 400);
    }

    /**
     * Make sure the page we are on
     * Is the same as the selected Header
     */
    this.syncMenuAndPage = function (currentPage)
    {
        //log(currentPage);
        //log('root.menubar ' + root.menubar);

        if (root.menubar == 'interests') {
            if (currentPage == PAGE_MY_INTERESTS) {
                $('#menu-interests .slick-container').slick('slickGoTo', 1);
            }

            if (currentPage == PAGE_EDIT_INTERESTS) {
                $('#menu-interests .slick-container').slick('slickGoTo', 3);
            }

            // on dashboard / hide the interests bar - go to articles
            if (currentPage == PAGE_DASHBOARD) {
                HEADER.toggleMenuBar('articles', 0);
                $('#menu-interests .slick-container').slick('slickGoTo', 2);
            }
            return false;
        }

        $('.slick-slide a').each(function ()
        {
            var pageMenu = 'page-' + $(this).attr('data-id');
            if (currentPage == pageMenu) {
                //log(pageMenu);
                //log(root.menubar);
            }
        })
    }

    /**
     * Init settings menubar slick
     */
    var initInterestsMenu = function ()
    {
        destroySlick('#menu-interests .slick-container');

        initSlick('interests', '#menu-interests .slick-container', 2);
    }

    /**
     * Initialize slick
     * @param element
     */
    var initSlick = function (type, element, initialSlide, arrows)
    {
        // when only 3 - the main menu
        $(element).on('init', function (event, slick)
        {
            if (type == 'interests') {
                for (var i = 0; i < slick.$slides.length; i++) {
                    var slide = $(slick.$slides[i]);

                    if (slide.attr('data-slick-index') == 0) {
                        slide.attr('data-slick-index', 3)
                    }
                    if (slide.attr('data-slick-index') == 4) {
                        slide.attr('data-slick-index', 1)
                    }
                }
            }
        });

        arrows = arrows ? arrows : false;
        var prevArrow = false;
        var nextArrow = false;
        if (arrows == true) {
            prevArrow = '<span class="slick-prev"><i class="icon-left"></i></span>';
            nextArrow = '<span class="slick-next"><i class="icon-right"></i></span>';
        }

        var options = {
            speed: 350,
            dots: false,
            swipe: false,
            arrows: arrows,
            slidesToShow: 3,
            infinite: false,
            draggable: false,
            touchMove: false,
            centerMode: true,
            slidesToScroll: 1,
            focusOnSelect: true,
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            centerPadding: '10px',
            initialSlide: initialSlide
        };

        if (type == 'articles') {
            options['slidesToShow'] = 1;
            options['swipe'] = true;
            options['draggable'] = true;
            options['touchMove'] = true;
            options['infinite'] = true;
            // options['centerPadding'] = '20%';
        }

        $(element).slick(options);
        $(element).on('afterChange', function (event, slick, currentSlide)
        {
            // find the menu with the slide index
            var slide = slick.$list.find('.slick-center');
            if (slide) {
                var link = slide.find('a');
                if (link) {
                    log('AFTER CHANGE - PAGE - ' + link.html());
                    link.trigger('tap');
                }
            }
        });

        registerHeaderPageLinks();
    }

    var destroySlick = function (element)
    {
        // destroy if already initialized
        if ($(element).hasClass('slick-initialized')) {
            $(element).slick('unslick');
        }
    }

    var registerEvents = function ()
    {
        $('#header #hamburger').off('tap');
        $('#header #hamburger').on('tap', function (e)
        {
            e.preventDefault();
            disableEnableButton($(this));

            // if already visible / selected
            if ($('#menu-interests').hasClass('active')) {
                root.toggleMenuBar('articles', 0);
            } else {
                root.toggleMenuBar('interests');
            }

            return false;
        });
    };

    var registerHeaderPageLinks = function ()
    {
        $('.header-page-link').off('tap');
        $('.header-page-link').on('tap', function (e)
        {
            var page = $(this).attr('data-id');
            goToPage(page, ANIMATION_IN, ANIMATION_OUT);

            // when dashboard - update the menubar
            if (page == 'dashboard') {
                HEADER.toggleMenuBar('articles');
            }
        });
    }

    /**
     * Register the articles links in menuber
     */
    var registerArticlesMenubar = function ()
    {
        $('.link-menubar-articles').off('tap');
        $('.link-menubar-articles').on('tap', function (e)
        {
            //killEvent(e);
            //disableEnableButton($(this));

            // get cat id
            var type = $(this).attr('data-type');
            var id = $(this).attr('data-article-id');
            SELECTED_ARTICLE_PAGE = $(this).attr('data-page-id');

            // if page already created
            var item = findItemInArray(SELECTED_ARTICLE_PAGE, ARTICLE_PAGES);

            // create new instance of articles page
            if (!item) {

                // find the interest - menu article id item
                var article = findItemInArray(id, INTERESTS.allInterests, ARTICLE_TYPES[type]['key']);

                var page = new ArticlesPage({
                    'type': type,
                    'url': ARTICLE_TYPES[type]['url'],
                    'idsKey': ARTICLE_TYPES[type]['key'],
                    'ids': article[ARTICLE_TYPES[type]['ids_key']],
                    'cacheKey': '4-' + SELECTED_ARTICLE_PAGE,
                    'container': $('#page-articles .article-list-box')
                });

                item = {'id': SELECTED_ARTICLE_PAGE, 'page': page};
                ARTICLE_PAGES.push(item);
            }

            item['page'].getAndShowItems(true);

            // go to page
            forcePageRedirect(PAGE_ARTICLES);
            $("html,body").animate({scrollTop: 1}, 300);
        })
    }

    this.construct(options);
}