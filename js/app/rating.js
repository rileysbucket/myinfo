var EVENT_ID = 1;
var EXHIBITOR_ID = 4;

function registerRating()
{
    $('.rating').on('change', function ()
    {
        var v = "-";
        var id = $(this).val();
        switch (id) {
            case "1":
                v = "Very Poor";
                break;
            case "2":
                v = "Poor";
                break;
            case "3":
                v = "Average";
                break;
            case "4":
                v = "Good";
                break;
            case "5":
                v = "Very Good";
                break;
        }

        var element = '#' + $(this).attr('data-label');
        $(element).html(v);
    });

    $('#btn-rating-submit').off('tap');
    $('#btn-rating-submit').on('tap', function (e)
    {
        var btn = $(this);
        stopSubmitButton(btn, e);

        var valid = true;
        var obj = {
            user_id: USER_ID,
            event_id: EVENT_ID,
            exhibitor_id: EXHIBITOR_ID,
            'rate-feedback': inputVal(PAGE_RATING, 'in-comment')
        };

        // rating elements on rating page only
        $('#' + PAGE_RATING + ' .rating').each(function ()
        {
            if ($(this).val() <= 0) {
                valid = false;
            }

            obj[$(this).attr('data-label')] = $(this).val();
        });

        if (valid == false) {
            showButtonAlertError($(this));
            return false;
        }

        doServerCall(URL_RATING, obj, function (response)
        {
            enableButton(btn);

            if (response['success'] == true) {
                resetRatingForm();
                addKeyValueToSession("user_points", response['user_points']);
                updateLoginInformation();
                showNotification("Thank you for the rating");

                PASSPORT.getLoyalties();
                VOUCHER.getVouchers();

                goToPageDelay(PAGE_PASSPORT, 1500);
            } else {
                showAlertError(btn.attr('data-feedback'), response.msg);
            }
        });

        return false;
    });
}

/**
 * Reset the registration form
 */
function resetRatingForm()
{
    $('#feedback-rating').hide();
    input(PAGE_RATING, 'in-comment').val('');

    input(PAGE_RATING, 'rating').attr('value', 0);
    input(PAGE_RATING, 'rating').rating('rate', 0);
    input(PAGE_RATING, 'rating-label-result').html('');
}