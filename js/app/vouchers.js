/*
 * VoucherClass
 */
var VoucherClass = function ()
{
    var root = this;

    root.vouchers = [];
    root.selectedVoucher = [];

    this.getVouchers = function (showLoader)
    {
        showLoader = showLoader ? showLoader : false;
        doServerCall(URL_VOUCHERS, null, function (response)
        {
            if (response['success'] != true) {
                showNowVouchers();
                return false;
            }

            root.vouchers = response['vouchers'];

            renderVouchers();
        }, showLoader);
    };

    /**
     * Render all the vouchers to the screen
     */
    var renderVouchers = function ()
    {
        $('#voucher-container .voucher').off('tap');
        $('#voucher-container').html('');
        for (var i = 0; i < root.vouchers.length; i++) {
            var cloned = $('#template-voucher').clone();
            var html = updateVoucherTemplate($(cloned.html()), root.vouchers[i]);

            $('#voucher-container').append(html);
        }

        // voucher badges
        $('#vouchers-badge').hide();
        if (root.vouchers.length > 0) {
            $('#vouchers-badge').html(root.vouchers.length);
            $('#vouchers-badge').fadeIn();
        }

        registerEvents();
    };

    /**
     * Set the information in the tempalte
     * @param html
     * @param data
     * @returns {*}
     */
    var updateVoucherTemplate = function (html, data)
    {
        var expire_at = moment(data['end_date'], MOMENT_FORMAT_FULL).format('D/MM/YYYY');

        html.attr('data-type', data['type']);
        html.attr('data-id', data['voucher_id']);
        html.find('.voucher-image img').attr('src', data['voucher_icon']);
        html.find('.voucher-title').html(data['title']);
        html.find('.voucher-subtitle').html(data['body']);
        html.find('.voucher-expire span').html(expire_at);

        return html;
    };

    var registerEvents = function ()
    {
        $('#voucher-container .voucher').on('tap', function ()
        {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');

            VOUCHER.selectedVoucher = findItemInArray(id, root.vouchers, 'voucher_id');

            if (type != 'touch_n_win') {
                claimVoucherServer();
            } else {
                if (VOUCHER.selectedVoucher['win_id'] > 0) {
                    claimVoucherServer();
                } else {
                    TOUCH_AND_WIN.reset();
                    goToPage(PAGE_TOUCH_AND_WIN);
                }
            }
        });
    }

    var claimVoucherServer = function ()
    {
        doServerCall(URL_CLAIM_VOUCHER, VOUCHER.selectedVoucher, function (response)
        {
            if (response['success'] != true) {
                showNotification(response['msg'], 'error');
            }

            $('#server-html-box').html(response['html']);
            goToPage(PAGE_SERVER_HTML);
        });
    }

    var showNowVouchers = function ()
    {
        $('#vouchers-badge').hide();
        $('#voucher-container').html('');

        var html = '<div>You do not have any vouchers.';
        html += '<a data-id="loyalties-information" class="page-link btn text-primary">';
        html += 'How it works</a></div>';
        $('#voucher-container').append(html);

        registerPageLinks();
    }
};