/*
 * MyClass
 */
var TouchAndWinClass = function (options)
{
    var vars = {
        myVar: 'original Value'
    };

    var root = this;
    root.prizesRevealed = 0;
    root.isActive = false;

    this.construct = function (options)
    {
        $.extend(vars, options);
    };

    this.reset = function ()
    {
        resetTouchWin();

        doServerCall(URL_CLAIM_VOUCHER, VOUCHER.selectedVoucher, touchAndWinReceived);
    }

    /*
     * Public method
     * Can be called outside class
     */
    var activate = function ()
    {
        $(".prize-wrapper").off('tap');
        $(".prize-wrapper").on('tap', function (e)
        {
            if (!root.isActive) {
                showNotification('Oops, The Touch and Win Game is not active.', 'error');
                return;
            }

            e.preventDefault();
            var id = $(this).attr('id');
            var fromClass = "pt-page-flipOutTop";
            var toClass = "pt-page-flipInBottom pt-page-delay400";

            animateTouchEle($(this), fromClass, toClass);
        });
    };

    var resetTouchWin = function (fromPage)
    {
        root.isActive = false;
        root.prizesRevealed = 0;

        if (fromPage == true) {
            var fromClass = "pt-page-rotateOutNewspaper";
            var toClass = "pt-page-rotateInNewspaper pt-page-delay500";

            // flip them back
            $(".prize-wrapper").each(function ()
            {
                $(this).attr('current', 1);
                $(this).attr('data-clicked', 'false');
                $(this).attr('data-isanimating', 'false');
                animateTouchEle($(this), fromClass, toClass);
            });
        } else {
            $(".prize-box").each(function ()
            {
                $(this).removeClass('current');
                $(this).attr('data-originalclasslist', $(this).attr('class'));
            });

            $(".prize-wrapper").each(function ()
            {
                $(this).attr('data-current', 0);
                $(this).attr('data-clicked', 'false');
                $(this).attr('data-isanimating', "false");
                $(this).children(".prize-box").first().addClass('current');
            });
        }

        $('#feedback-touch-win-play').hide();
    }

    var touchAndWinReceived = function (response)
    {
        if (response['success'] != true) {
            root.isActive = false;
            showAlertError('feedback-touch-win-play', 'Oops, something went wrong, please try again');
            return;
        }

        root.prizesRevealed = 0; // total prices revealed / clicked
        root.isActive = true; // game is active
        addKeyValueToSession("user_points", response['user_points']);

        $("#touch-win-play-id").val('' + response['play_id']);
        $("#touch-win-play-result").val(response['play_result']);
        $("#touch-win-play-hash").val(response['play_hash']);

        $("#prize-wrapper-1 .prize-image").attr('src', response['image_1']);
        $("#prize-wrapper-2 .prize-image").attr('src', response['image_2']);
        $("#prize-wrapper-3 .prize-image").attr('src', response['image_3']);

        activate();
        VOUCHER.getVouchers();
    }

    var animateTouchEle = function (wrapper, fromClass, toClass)
    {
        if (wrapper.attr('data-isanimating') == 'true') {
            return;
        }

        // get the current index
        var current = wrapper.attr('data-current');
        var elements = wrapper.children('.prize-box');

        var fromEle = $(elements.get(current));
        if (++current >= elements.length) {
            current = 0;
        }
        var toEle = $(elements.get(current));

        wrapper.attr('data-current', current);
        wrapper.attr('data-isanimating', 'true');
        wrapper.attr('data-clicked', (current == 1 ? 'true' : 'false'));

        fromEle.addClass(fromClass);
        toEle.addClass(toClass);
        toEle.addClass('current');

        setTimeout(function ()
        {
            resetAnimateTouchEle(wrapper, fromEle, toEle);
        }, 1000);
    }

    var resetAnimateTouchEle = function (wrapper, fromEle, toEle)
    {
        wrapper.attr('data-isanimating', "false");
        fromEle.attr('class', fromEle.attr('data-originalclasslist'));
        toEle.attr('class', toEle.attr('data-originalclasslist') + ' current');

        root.prizesRevealed = 0;
        $(".prize-wrapper").each(function ()
        {
            if ($(this).attr('data-clicked') == 'true') {
                root.prizesRevealed++;
            }
        });

        handleTouchRevealedClick();
    }

    var handleTouchRevealedClick = function ()
    {
        if (root.prizesRevealed >= 3) {
            root.isActive = false;

            var level = "error";
            var title = "Almost, ";
            var desc = " Better luck next time.";
            if ($("#touch-win-play-result").val() == "win") {
                level = "success";
                title = "Congratulations!";
                desc = " You have won.";

                /*doServerCall(BASE_PATH_EVENTS + URL_TOUCH_WIN_SUBMIT, {
                    "user_id": USER_ID,
                    "play_id": $("#touch-win-play-id").val(),
                    "play_hash": $("#touch-win-play-hash").val()
                }, function (response)
                {
                    log('TOUCH REVEALED RESPONSEEEE -----------------');
                    log(response);
                    log('TOUCH REVEALED RESPONSEEEE -----------------');
                });*/
            }

            setTimeout(function ()
            {
                showNotification(title + desc, level);

                goToPageDelay(PAGE_VOUCHERS);
            }, 500);
        }
    }

    this.construct(options);
};