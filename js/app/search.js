/*
 * SearchClass
 */
var SearchClass = function (options)
{
    var vars = {
        myVar: 'original Value'
    };

    var root = this;

    this.construct = function (options)
    {
        $.extend(vars, options);
    };

    this.init = function ()
    {
        registerEvents();
    };

    var registerEvents = function ()
    {
        $('#header-search').on('focus', function ()
        {
            showNotification('This Feature will launch soon', 'info');
        });
    };

    this.construct(options);
};