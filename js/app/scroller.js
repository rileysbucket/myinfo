/*
 * SCROLLER AND LISTEN FOR EVENTS
 */
var ScrollClass = function (options)
{
    var vars = {
        element: 'body'
    };

    var root = this;

    root.scrollPrev = 1;
    root.scrollZeroCount = 0;

    this.construct = function (options)
    {
        $.extend(vars, options);
    };

    this.registerEvents = function ()
    {
        root.deRegisterEvents();
        $(vars.element).on('touchmove', listenToScroll);
    }

    this.deRegisterEvents = function ()
    {
        $(vars.element).off('touchmove');
    }

    var listenToScroll = function (e)
    {
        var scrolledFromTop = $(this).scrollTop();
        var height = $(window).height();

        // if (scrolledFromTop > 40) {
        //     HEADER.toggleHeaderScroll('hide');
        // } else if (scrolledFromTop < 20) {
        //     HEADER.toggleHeaderScroll('show');
        // }

        // if at bottom of page
        if (scrolledFromTop + height >= $(document).height() - 100) {
            // dashboard
            if (PAGE_SELECTED == PAGE_DASHBOARD) {
                DASHBOARD.getAndShowItems();
            }

            // categories
            if (PAGE_SELECTED == PAGE_ARTICLES) {
                var item = findItemInArray(SELECTED_ARTICLE_PAGE, ARTICLE_PAGES);
                if (item) {
                    item['page'].getAndShowItems();
                }
            }
        }

        // on dashboard of articles, if longer than 2 sec at the top - get new items
        if (PAGE_SELECTED == PAGE_DASHBOARD || PAGE_SELECTED == PAGE_ARTICLES) {
            if (root.scrollPrev == 0 && scrolledFromTop <= 0) {
                setTimeout(function ()
                {
                    // still at the top - get new items
                    if (root.scrollPrev == 0) {
                        root.scrollPrev = 1; // prevent duplicate fire
                        DASHBOARD.getAndShowItems(true, true);
                        $("html,body").animate({scrollTop: 1}, 100);
                    }
                }, 2000);
            }
            else {
                root.scrollZeroCount = 0;
            }
        }

        root.scrollPrev = scrolledFromTop;
    }

    /*
     * Pass options when class instantiated
     */
    this.construct(options);

};