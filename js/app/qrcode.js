function registerQrCode()
{
    $('#footer-qrcode').on('tap', function (e)
    {
        killEvent(e);
        disableEnableButton($(this));

        if (!checkAuthGuard('Unfortunately you have to be signed in to use the QR code scanner.')) {
            return false;
        }

        if (!IS_DESKTOP) {
            onQRCodeStart(onQRCodeSuccess);
        } else {
            onQRCodeSuccess({text: 'https://events.my.na/app/rating/105/INTOUCH INTERACTIVE MARKETING/26'});
            // onQRCodeSuccess({text: 'SMSTO:+264818254455:This is a SMS stored in a QR Code!'});
        }

        return false;
    });
}

/**
 * QR Code Success
 * @param response
 * @returns {boolean}
 */
function onQRCodeSuccess(response)
{
    var text = response.text;
    var url = decodeURIComponent(response.text);

    if (response['cancelled'] == true) {
        return false;
    }

    // URL
    if (url.indexOf('http://') >= 0 || url.indexOf('https://') >= 0) {

        // events rating
        if (url.indexOf('https://events.my.na/app/rating/') >= 0) {

            var value = url.substr('https://events.my.na/app/rating/'.length);

            var pieces = value.split('/');
            if (pieces.length >= 3) {
                EVENT_ID = pieces[0];
                EXHIBITOR_ID = pieces[2];
                $('#' + PAGE_RATING + ' .header').html(pieces[1]);

                goToPage(PAGE_RATING);
            } else {
                openQRUrlInBrowser(url);
            }
        } else {
            openQRUrlInBrowser(url);
        }

        return false
    }

    // check if sms
    if (text.toLowerCase().indexOf('smsto:') >= 0) {
        var pieces = text.split(':');
        if (pieces.length >= 2) {
            sendNativeSMS(pieces[1], pieces[2])
            return false;
        }
    }

    showNotification('Could not verify the QR Code', 'error');

    return false;
}

/**
 * Open the url scanned in a app browser
 * Append params before open
 * @param url
 */
function openQRUrlInBrowser(url)
{
    var final = appendGetParamsToUrl(url);
    openInAppBrowser(final);
}

function appendGetParamsToUrl(url)
{
    url = addParameterToUrl(url, 'client_id', USER_ID);
    //url = addParameterToUrl(url, 'user_gender', getValueFromSession('user_gender'));

    return url;
}

/**
 * Append parameter to a url
 * @param url
 * @param parameterName
 * @param parameterValue
 * @param atStart - if it needs to be first
 * @returns {string}
 */
function addParameterToUrl(url, parameterName, parameterValue, atStart)
{
    var replaceDuplicates = true;
    if (url.indexOf('#') > 0) {
        var cl = url.indexOf('#');
        var urlhash = url.substring(url.indexOf('#'), url.length);
    } else {
        var urlhash = '';
        cl = url.length;
    }
    var sourceUrl = url.substring(0, cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1) {
        var parameters = urlParts[1].split("&");
        for (var i = 0; (i < parameters.length); i++) {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] == parameterName)) {
                if (newQueryString == "") {
                    newQueryString = "?";
                } else {
                    newQueryString += "&";
                }
                newQueryString += parameterParts[0] + "=" + (parameterParts[1] ? parameterParts[1] : '');
            }
        }
    }
    if (newQueryString == "") {
        newQueryString = "?";
    }

    if (atStart) {
        newQueryString = '?' + parameterName + "=" + parameterValue + (newQueryString.length > 1 ? '&' + newQueryString.substring(1) : '');
    } else {
        if (newQueryString !== "" && newQueryString != '?') {
            newQueryString += "&";
        }
        newQueryString += parameterName + "=" + (parameterValue ? parameterValue : '');
    }
    return urlParts[0] + newQueryString + urlhash;
};