/* 
 * Library App Functions 
 * Ben-Piet O'Callaghan
 * 13-05-2014 (created at)
 * 30-02-2015 (updated at)
 * 0.3
 */

var PAGE_PREV = "";
var PAGE_SELECTED = "page-landing";
var PAGE_QUEUE = ["page-landing"];

var FORCE_SHOW_LOADER = false;

var DEBUG = true;
var IS_APPLE = false;
var IS_DESKTOP = true;

var DEVICE_TOKEN = 0;

var PREV_PAGE_ANIMATE = false;
var IS_PAGE_BUSY_ANIMATING = false;
var IS_PAGE_WAITING_FOR_DELAY = false;

var GOOGLE_MAPS_QUERY = 'https://maps.google.com?q=';
var GOOGLE_DRIVE_VIEW = 'http://docs.google.com/viewer?url='; // embedded=true&

function initLibrary()
{
    if (!IS_DESKTOP) {
        IS_APPLE = isiPhone();
    }

    //IS_APPLE = true;

    if (IS_APPLE) {
        $("#header").addClass('apple');
        $(".et-page").addClass('apple');
    }

    //initGeolocation();
    //updateDateTime();

    // events
    document.addEventListener('swiperight', onBackKeyDown);
    document.addEventListener("backbutton", onBackKeyDown, false);
    document.addEventListener("offline", onNetworkConnectionLost, false);
    document.addEventListener("online", onNetworkConnectionConnected, false);

    // go back
    $(".back-link").on('tap', function (e)
    {
        killEvent(e);

        onBackKeyDown(e);
    });

    // set original class list - used when reset icon
    $('.btn-spin').each(function ()
    {
        if ($(this).find('i')) {
            $(this).attr('data-spin-icon', $(this).find('i').attr('class'));
        }
    });

    // for some random reason - i need to reset them
    setTimeout(function ()
    {
        $('.btn-spin').each(function ()
        {
            if ($(this).find('i')) {
                $(this).find('i').attr('class', $(this).attr('data-spin-icon'));
            }
        });
    }, 2000);

    // add the refresh icon and animate it on tap
    $('.btn-spin').on('tap', function (e)
    {
        if ($(this).attr('data-spin-icon')) {
            var iconList = $(this).attr('data-spin-icon');
            var icon = $(this).find('i');
            icon.removeClass();
            icon.addClass('fa fa-spin fa-refresh');
            $(this).prop('disabled', true);
        }
    });

    // go back or logout
    $(".back-logout-link").on('tap', function (e)
    {
        killEvent(e);

        if (PAGE_SELECTED == PAGE_DASHBOARD) {
            confirmLogout();
        } else {
            onBackKeyDown(e);
        }
    });

    registerPageLinks();
    registerExternalLinks();
    registerShareLinks();

    $(document).ajaxStart(function ()
    {
        Pace.restart();
    });

    PageTransitions.init();
}

function registerPageLinks()
{
    $('.page-link').off('tap');

    // go to a page
    $('.page-link').on('tap', function (e)
    {
        killEvent(e);
        var page = $(this).attr('data-id');
        if (page == "logout") {
            confirmLogout();
        } else if (page != "false") {
            if ($(this).attr('data-prev')) {
                return goToPrevUrl();
            }
            goToPage(page, ANIMATION_IN, ANIMATION_OUT);
        }

        return false;
    });
}

function registerExternalLinks()
{
    $(".external-link").off('tap');

    // open the in app browser
    $(".external-link").on('tap', function (e)
    {
        killEvent(e);
        openExternalUrl($(this).attr('data-href'));
        return false;
    });
}

function openExternalUrl(url)
{
    if (navigator && navigator.app) {
        navigator.app.loadUrl(url, {openExternal: true});
    } else {
        window.open(url, '_blank', 'location=yes');
    }
}

function openInAppBrowser(url)
{
    if(IS_DESKTOP) {
        openExternalUrl(url);
    } else {
        cordova.InAppBrowser.open(url, '_blank', 'location=no');
    }
}

function registerShareLinks()
{
    $('.btn-share').off('tap');
    $('.btn-share').on('tap', function (e)
    {
        killEvent(e);

        var btn = $(this);
        var title = DASHBOARD.selectedArticle['title'];
        var description = '';
        var link = 'https://www.my.na/';
        var type = $(this).attr('data-type');
        var message = title + ' ' + description;

        if (IS_DESKTOP == false) {
            if (type == 'twitter_share') {
                window.plugins.socialsharing.shareViaTwitter(message, null /* img */, link, onShareSuccess, onShareFail);
            }
            if (type == 'whatsapp') {
                window.plugins.socialsharing.shareViaWhatsApp(message, null /* img */, link, onShareSuccess, onShareFail);
            }
            if (type == 'facebook_share') {
                window.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(message, null /* img */, link, '', onShareSuccess, onShareFail);
            }
        }

        doServerCall(URL_SOCIAL_MEDIA, {
            'type_id': DASHBOARD.selectedArticle['post_id'],
            'type': 'post',
            'social_type': $(this).attr('data-type')
        }, function (response)
        {
            if (response['msg']) {

                var total = parseInt(btn.find('span').html()) + 1;
                btn.find('span').html(total);
                showNotification(response['msg']);
            }
        }, false);
    });
}

function onShareSuccess()
{
    //showPopup('Thank You', 'Thank you for sharing our app with your friends');
}

function onShareFail(errormsg)
{
    showPopup('Sorry', 'We could not find the required app on your device');
}

/* Back btn on cellphone */
function onBackKeyDown(e)
{
    killEvent(e);

    // if we are on home - ask to close
    switch (PAGE_SELECTED) {
        case PAGE_LANDING:
        case PAGE_DASHBOARD:
            confirmExit();
            break;
        case PAGE_FEEDBACK:
            //goToPage(PAGE_DASHBOARD, ANIMATION_BACK_IN, ANIMATION_BACK_OUT);
            break;
        default:
            goToPrevUrl();
    }
}

function confirmLogout()
{
    showPopupConfirm('Logout ', 'Are you sure you want to logout?', function (buttonIndex)
    {
        if (buttonIndex == 1) {
            doLogout();
        }
    }, ['LOGOUT', 'CANCEL']);

    if (IS_DESKTOP) {
        doLogout();
    }
}

/**
 * Confirm Exit
 * - on desktop - logout
 */
function confirmExit()
{
    if (!navigator.notification) {
        confirmLogout();
    } else {
        navigator.notification.confirm("Are you sure you want to exit?", onConfirmExit, "Confirmation", "Yes, No");
    }
}

function onConfirmExit(button)
{
    if (button == 2) { // If User selected No, then we just do nothing
        return;
    } else {
        navigator.app.exitApp(); // Otherwise we quit the app.
    }
}

function log(value)
{
    if (DEBUG && IS_DESKTOP) {
        console.log(value);
    }
    // if(DEBUG) alert(value);
}

function goToPageLibrary(pageId, inClass, outClass)
{
    // if we are busy goint to page, waiting for delay, or already on that page
    if (IS_PAGE_BUSY_ANIMATING == true || IS_PAGE_WAITING_FOR_DELAY == true || PAGE_SELECTED == pageId) {
        return;
    }

    inClass = (typeof inClass === "undefined") ? ANIMATION_IN : inClass;
    outClass = (typeof outClass === "undefined") ? ANIMATION_OUT : outClass;

    // if we navigate to a prev page, dont add it to queue
    if (!PREV_PAGE_ANIMATE) {
        PAGE_QUEUE.push(pageId);
    }

    //console.log(PAGE_QUEUE);
    //console.log('------------------');

    PREV_PAGE_ANIMATE = false;
    IS_PAGE_BUSY_ANIMATING = true;

    // hide the header bar when on these pages
    if (pageId == PAGE_LANDING ||
        pageId == PAGE_LOGIN ||
        pageId == PAGE_REGISTER ||
        pageId == PAGE_FORGOT_PASSWORD) {
        showHeaderFooter(false);
    } else {
        showHeaderFooter(true);
    }

    // on dashboard hide the buttons
    if (pageId == PAGE_DASHBOARD) {
        //$('#menu a.back').fadeOut();
        $('#menu a.back').html('<i class="fa fa-sign-out"></i> <span>Logout</span></a>');
    }
    // on the feedback pages, hide the back button
    else if (pageId == PAGE_FEEDBACK) {
        $('#menu a.back').fadeOut();
    }
    else // all other pages, show buttons
    {
        $('#menu a.back').fadeIn();
        $('#menu a.back').html('<i class="fa fa-mail-reply"></i> <span>Back</span></a>');
    }

    PAGE_PREV = PAGE_SELECTED;
    PAGE_SELECTED = pageId;
    animateToPage(pageId, inClass, outClass);

    // scroll to top after 500ms
    setTimeout(function ()
    {
        HEADER.toggleHeaderScroll('show');

        var scrollTo = 0;
        // to go back to ur selected article
        if (PAGE_PREV == PAGE_SHOW_ARTICLE &&
            (PAGE_SELECTED == PAGE_DASHBOARD ||
            PAGE_SELECTED == PAGE_ARTICLES)) {
            scrollTo = DASHBOARD.scrollPosition;
        }

        $("html,body").animate({scrollTop: scrollTo}, 300);

        // set busy animating to false after 400ms
        setTimeout(function ()
        {
            IS_PAGE_BUSY_ANIMATING = false;
        }, 400);
    }, 500);

    log("GO TO PAGE - " + pageId);
    log(PAGE_QUEUE);
}

function animateToPage(pageId, inClass, outClass)
{
    PageTransitions.animateToPage($("#wrapper"), $("#" + pageId), inClass, outClass);
}

/* Ajax Calls to get the relevant information */
function doServerCall(url, data, callback, showLoading, type)
{
    type = type ? type : 'POST';

    if (data == undefined || data == null) {
        data = {};
    }
    if (showLoading == undefined || showLoading == true) {
        showLoader();
    }

    // always send data with these params
    data.user_id = USER_ID;
    data.client_id = USER_ID;
    data.session_token = SESSION_TOKEN;
    data.platform = getPlatform();
    data.app_version = APP_VERSION;

    // check connection to do ajax
    if (isNetworkConnected()) {
        doAjax(url, data, callback, type);
    }
    else // no network, but we require network for the calls
    {
        //showPopup('Internet', 'Internet is required for these actions, please restart app with internet.');
        return;
    }
}

function doAjax(url, data, callback, type)
{
    type = type ? type : 'POST';

    if (data === undefined || data == null) {
        data = {};
    }

    var urlFull = BASE_PATH + url;
    if (url.search('http://') >= 0 || url.search('https://') >= 0) {
        urlFull = url;
    }

    log("AJAX - " + urlFull);
    logger('call: ' + urlFull);

    $.ajax({
        type: type,
        url: urlFull,
        data: data,
        dataType: "json",
        timeout: 90000,
        error: function (xhr)
        {
            log('error');
            log(xhr);
            log(xhr.responseText);
            log(xhr.responseJSON);

            return false;

            // if we have a callback function
            if (typeof callback == 'function') {
                callback(data.responseJSON);
            }

            //hideLoader();
            //resetSpinnerButton('.btn-spin');
            //showPopup('Internet', 'Please make sure you are connected to the internet.');
            if (t === "timeout") {
                //showPopup('Internet', 'Please make sure you are connected to the internet.');
            }
        },
        success: function (response)
        {
            handleAjaxSuccess(urlFull, response, callback);
        }
    });
}

function handleAjaxSuccess(url, response, callback)
{
    hideLoader();
    resetSpinnerButton('.btn-spin');

    log("handleAjaxSuccess: URL --  " + url);

    if (response) //  && response.result  && isEmpty(response) == false
    {
        // show the alert popup (dont return, callback can wait for call
        if (response.error && response.type == 'popup' && (response.error.title || response.error.content)) {

            hideLoader();
            resetSpinnerButton('.btn-spin');

            if (response.error.content.indexOf('session has expired') > -1) {

                if (USER_IS_LOGGED_IN == true) {
                    doLogout();
                    USER_IS_LOGGED_IN = false;
                    showPopup(response.error.title, response.error.content);

                    setTimeout(function ()
                    {
                        goToPageDelay(PAGE_LOGIN);
                    }, 1300);
                }

                return false;
            } else {
                showPopup(response.error.title, response.error.content);
            }
        }

        // if we have a callback function
        if (typeof callback == 'function') {
            callback(response);
        }
    }

    // log($.parseJSON(response));
    //log(response);
    if (response && response.data) {
        logger('received ' + url + ' - ' + (response.data.length));
    }

    $('#logger-top-right').html('ajax - ' + url);
}

function getArticlesNMH(url, data, callback)
{
    if (!isNetworkConnected()) {
        if (typeof callback == 'function') {
            callback('no-network');
        }
        return false
    }

    showLoader();

    $.ajax({
        type: 'POST',
        url: BASE_PATH + url,
        data: data,
        dataType: "json",
        timeout: 90000,
        success: function (response)
        {
            hideLoader();
            if (typeof callback == 'function') {
                callback(response);
            }
        }
    });
}

function logger(value)
{
    if (DEBUG) {
        var html = $("#logger .list").html();
        $("#logger .list").html('<p>' + value + '</p>' + html);
    }
}

function showHeaderFooter(open)
{
    addOpenCloseClass("header", open);
    addOpenCloseClass("footer", open);
}

function showLoader(force)
{
    if (force && force == true) {
        FORCE_SHOW_LOADER = true;
    }

    $("#loading-box").fadeIn();
}

function hideLoader(force)
{
    if (force && force == true) {
        FORCE_SHOW_LOADER = false;
    }

    // only fadeout when we may
    if (!FORCE_SHOW_LOADER) {
        $("#loading-box").fadeOut();
        setLoadingText('Loading');
    }
}

function isLoaderVisible(visible)
{
    if (visible == undefined || visible == true) {
        return $('#loading-box').is(':visible');
    } else {
        return !$('#loading-box').is(':visible');
    }
}

function setLoadingText(value)
{
    // can't change the title when forced
    // if(FORCE_SHOW_LOADER) { return; }

    if (!(value && value.length > 2)) {
        value = 'Loading';
    }

    $("#loading-title").html(value);
}

function getLoadingText()
{
    return $("#loading-title").html();
}

function showHideElement(ele, show)
{
    $$(ele).removeClass("show");
    $$(ele).removeClass("hide");

    if (show) {
        $$(ele).addClass("show");
    }
    else {
        $$(ele).addClass("hide");
    }
}

function addOpenCloseClass(ele, open)
{
    if (open) {
        $('#' + ele).removeClass("close");
        $('#' + ele).addClass("open");
    } else {
        $("#" + ele).removeClass("open");
        $("#" + ele).addClass("close");
    }
}

function isiPhone()
{
    return (device && device.platform && device.platform.toLocaleLowerCase().indexOf('ios') >= 0) ? true : false;
    return (
        (navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPod") != -1)
    );
}

function showRequireNetworkAlert()
{
    showPopup("Oops", "Unfortunately this action requires internet access. Please connect to a network");
}

function showPopup(title, desc)
{
    if (!navigator.notification) {
        alert(title + ' - ' + desc);
    } else {
        navigator.notification.alert(desc, function ()
        {
        }, title, 'OK');
    }
}

function showPopupConfirm(title, message, callback, buttonLabels)
{
    if (!navigator.notification) {
        return confirm(title + ' - ' + message);
    } else {
        buttonLabels = (buttonLabels ? buttonLabels : ['OK', 'CANCEL']);
        navigator.notification.confirm(message, callback, title, buttonLabels);

        return 'notification';
    }
}

function saveDeviceLocation()
{
    // wait 10 seconds before we update the location
    setTimeout(function ()
    {
        //alert("saving gps " + _geolocation['latitude']);
        doServerCall(URL_LOCATION, _geolocation, null, false);
    }, 10000);
}