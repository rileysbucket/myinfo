//-------------------------------------------------
// Connection To server
//-------------------------------------------------
function onNetworkConnectionLost()
{
    //showPopup('No Network', 'You are no longer connected to the internet.');
}
function onNetworkConnectionConnected()
{
    // app is still busy starting up, no need to sync yet
    if (DEVICE_READY == true) {
        updateSyncButtonClass();
        //checkNetworkForSyncing();
    }
}
function isNetworkConnected()
{
    if (IS_DESKTOP) {
        return true;
    }

    // if we have interwebs
    if (navigator && navigator.connection) {
        if ("none" == navigator.connection.type || "unknown" == navigator.connection.type) {
            return false;
        }
    }
    return true;
}
//-------------------------------------------------
// Local Storage / Session
//-------------------------------------------------
function validateSession(remove, redirect)
{
    // clear session data
    if (remove) {
        simpleStorage.flush();

        USER_SKIPPED = false;
        USER_IS_LOGGED_IN = false;
        USER_ID = 0;

        // clear the user js vars
        updateLoginStatus();
    }
    // if user is not logged in and we need a valid user to access this url
    var logged_in = simpleStorage.get("logged_in");
    if (logged_in == null || logged_in == undefined || logged_in == false) {
        if (redirect != undefined && redirect == true) {
            forcePageRedirect(PAGE_LANDING);
        }
        return false;
    }
}
function addKeyValueToSession(key, value)
{
    simpleStorage.set(key, value);
}

function saveLocalData(key, value)
{
    simpleStorage.set(key, value);
}

function cacheData(key, value)
{
    simpleStorage.set(key, value);
}

function cacheDataTimestamp(key)
{
    saveLocalData(key + LOCAL_DATA_SUFFIX, moment().toString());
}

function deleteCacheData(key)
{
    simpleStorage.deleteKey(key);
}

/**
 * Check if we can take the local data
 *
 * @param id
 * @returns {boolean}
 */
function checkCachedData(key, hours)
{
    hours = hours ? hours : 6;
    var date = getValueFromSession(key + LOCAL_DATA_SUFFIX);
    var diff = moment().diff(moment(date), 'hours');

    // data is not older than x hours
    if (isNumber(diff) && diff < hours) {
        return true;
    }

    // no internet
    if (!isNetworkConnected()) {
        // showPopup("Network", "Please connect to the internet to get the latest information.");
        return true;
    }

    // first time
    // old cached data
    // getting new informaiton
    return false;
}

function getValueFromSession(key, placeholder)
{
    var value = simpleStorage.get(key);
    if (typeof value === 'undefined' || value == null) {
        return (placeholder == undefined ? '' : placeholder);
    }
    return value;
}

function getScoreGrade(score)
{
    if (score >= 80) {
        return 'A';
    }
    else if (score >= 70) {
        return 'B';
    }
    else if (score >= 60) {
        return 'C';
    }
    else if (score >= 50) {
        return 'D';
    }
    else if (score >= 40) {
        return 'E';
    }
    else if (score < 40) {
        return 'F';
    }

    return 'n/a';
}
/* Local Storage - END */

//-------------------------------------------------
// UTILS
//-------------------------------------------------
function resetLoader()
{
    FORCE_SHOW_LOADER = false;
    hideLoader();
    setLoadingText();
}
function setHtmlAsNaIfEmpty(selector)
{
    var html = $(selector).html();
    if (html.length <= 1) {
        $(selector).html('n/a');
    }
}
function setSelectedObjFromArr(id, arr, obj)
{
    for (var i = 0; i < arr.length; i++) {
        if (id == arr[i]['id']) {
            obj = arr[i];
        }
    }

    return obj;
}

function findItemInArray(id, arr, key)
{
    key = key ? key : 'id';

    for (var i = 0; i < arr.length; i++) {
        if (id == arr[i][key]) {
            return arr[i];
        }
    }

    return false;
}

function logObjectProperties(obj)
{
    for (var property in obj) {
        alert("LOG: " + property + " -- " + obj[property]);
    }
}
String.prototype.trim = function ()
{
    return this.replace(/^\s+|\s+$/g, "");
}
String.prototype.ucFirst = function ()
{
    return this.charAt(0).toUpperCase() + this.slice(1);
}
function isString(n)
{
    return (typeof n == "string" ? true : false);
}
function isNumber(n)
{
    return !isNaN(parseFloat(n)) && isFinite(n);
}
var hasOwnProperty = Object.prototype.hasOwnProperty;
function isEmpty(value)
{
    // null and undefined are "empty"
    if (value == null) {
        return true;
    }
    if (isNumber(value) && value.toString().length > 0 && value > 0) {
        return false;
    }
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (value.length > 0) {
        return false;
    }
    if (value.length === 0) {
        return true;
    }
    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in value) {
        if (hasOwnProperty.call(value, key)) {
            return false;
        }
    }
    return true;
}
function encodeString(value)
{
    var encodedStr = value.replace(/[\u00A0-\u9999<>\&]/gim, function (i)
    {
        return '&#' + i.charCodeAt(0) + ';';
    });
    return encodedStr;
}
function filterList(list, key, match)
{
    var found = [];
    for (var i = 0, l = list.length; i < l; i++) {
        if (list[i][key].toLowerCase().search(match) >= 0) {
            found.push(list[i]);
        }
    }
    ;
    return found;
}
function getImageSizeByAuto(width, height, widthNew, heightNew)
{
    var widthOptimal = widthNew;
    var heightOptimal = heightNew;
    if (width > height) // image is wider (landscape)
    {
        heightOptimal = getSizeByFixedWidth(width, height, widthNew);
    }
    else if (width < height) // image is taller (portrait)
    {
        widthOptimal = getSizeByFixedHeight(width, height, heightNew);
    }
    else // image is a square
    {
        if (heightNew < widthNew) {
            heightOptimal = getSizeByFixedWidth(width, height, widthNew);
        } else if (heightNew > widthNew) {
            widthOptimal = getSizeByFixedHeight(width, height, heightNew);
        }
    }
    return {'width': widthOptimal, 'height': heightOptimal};
}
function getSizeByFixedHeight(width, height, heightNew)
{
    var ratio = width / height;
    var widthNew = heightNew * ratio;
    return widthNew;
}
function getSizeByFixedWidth(width, height, widthNew)
{
    var ratio = height / width;
    var heightNew = widthNew * ratio;
    return heightNew;
}
function getPercentage(value, total)
{
    if (value == 0 && total == 0) {
        return '100%';
    }
    if (value > total) {
        value = total;
    }
    return Math.round((((value / total) * 100) * 100) / 100) + '%';
}
//-------------------------------------------------
// DEVICE
//-------------------------------------------------
function getDeviceInformation()
{
    if (IS_DESKTOP) {
        return {
            'cordova': '',
            'model': '',
            'platform': 'desktop',
            'uuid': '',
            'version': ''
        };
    }
    return {
        'cordova': device.cordova,
        'model': device.model,
        'platform': device.platform,
        'uuid': device.uuid,
        'version': device.version
    };
}
function getPlatform()
{
    var d = getDeviceInformation();
    return d['platform'];
}
//-------------------------------------------------
// FORMS 
//-------------------------------------------------
function resetSpinnerButton(element)
{
    if ($(element).attr('data-spin-icon')) {
        var classList = $(element).attr('data-spin-icon');
        var icon = $(element).find('i');
        icon.removeClass();
        icon.addClass(classList);
        $(element).prop('disabled', false);
    }
}
function addButtonSpinner(element, fromIcon, toIcon)
{
    if (toIcon == undefined || toIcon == null) {
        toIcon = 'fa-spinner';
    }
    element.removeClass(fromIcon);
    element.addClass(toIcon);
    element.addClass('fa-spin');
}
function showButtonAlertError(element, title, description)
{
    enableButton(element);

    title = title ? title : 'Please complete the form';
    description = description ? description : '';

    showAlertError(element.attr('data-feedback'), title, description);
}
function showButtonAlertSuccess(element, title, description)
{
    enableButton(element);

    title = title ? title : 'Success';
    description = description ? description : '';

    showAlert(element.attr('data-feedback'), 'success', title, description);
}
function showAlertError(id, title, description)
{
    showAlert(id, 'error', title, description, 'warning');
}
function showAlert(id, type, title, description, icon)
{
    $("#" + id).removeClass('alert-error');
    $("#" + id).removeClass('alert-success');
    $("#" + id).addClass('alert-' + type);
    $("#" + id).html(title);
    //$("#" + id).html('<i class="fa fa-' + icon + ' fa-lg"></i><strong>' + title + '</strong> ' + description);
    //$("#" + id).css('display', "block !important");
    $("#" + id).fadeIn(300);
}
function showFadeOutAlert(id, type, title, description, icon)
{
    showAlert(id, type, title, description, icon);
    setTimeout(function ()
    {
        $("#" + id).fadeOut(300);
    }, 4000);
}
function setAlert(id, type, title, desc, show, fadeOut)
{
    $("#" + id).removeClass('alert-error');
    $("#" + id).removeClass('alert-success');
    $("#" + id).removeClass('alert-warning');
    $("#" + id).addClass('alert-' + type);
    $("#" + id).html('<strong>' + title + '</strong> ' + desc);
    if (show == undefined || show == true) {
        $("#" + id).css('display', "block");
    }
    // if(fadeOut == undefined || fadeOut == true) { setTimeout(function() { $$("#"+id).hide(); }, 7000); }
}
function showErrorAlert(id, content)
{
    content = (content == undefined ? '<strong>Oops!</strong> Please complete the form.' : content);
    $("#" + id).removeClass('alert-error');
    $("#" + id).removeClass('alert-success');
    $("#" + id).removeClass('alert-warning');
    $("#" + id).addClass('alert-error');
    $("#" + id).html(content);
    $("#" + id).css('display', "block");
}
function testInput(formStyle, inputName, key)
{
    var s = "";
    if ($("." + formStyle + " input[name='" + inputName + "']").val().length <= 0) {
        s += "Please add your " + key + ". <br/>";
    }
    return s;
}
var getMonths = function ()
{
    return [
        {'value': '01', 'label': 'January'},
        {'value': '02', 'label': 'February'},
        {'value': '03', 'label': 'March'},
        {'value': '04', 'label': 'April'},
        {'value': '05', 'label': 'May'},
        {'value': '06', 'label': 'June'},
        {'value': '07', 'label': 'July'},
        {'value': '08', 'label': 'August'},
        {'value': '09', 'label': 'September'},
        {'value': '10', 'label': 'October'},
        {'value': '11', 'label': 'November'},
        {'value': '12', 'label': 'December'}
    ];
}
var input = function (page, classs)
{
    return $('#' + page + ' .' + classs);
}
var inputVal = function (page, classs)
{
    return input(page, classs).val().trim();
}
var inputReset = function (page, classs)
{
    return input(page, classs).val('');
}
var selectVal = function (page, classs)
{
    return input(page, classs).find(":selected").attr('value');
}
var selectText = function (page, classs)
{
    return input(page, classs).find(":selected").text();
}
var roundValue = function (value)
{
    return Math.round(parseFloat(value) * 100) / 100;
}
function getSelectedRadio(page, name)
{
    var radios = ($('#' + page + " input[name='" + name + "']:checked"));
    if (radios.length > 0) {
        return radios.val();
    }
    return false;
}
var formatNumber2Decimal = function (value)
{
    value = parseFloat(value).toFixed(2);
    //value = (value).replace(/.00+$/, "");
    return value;
}
function validateInput(element, len)
{
    len = (len == undefined ? 2 : len);
    if (element.val().length <= len) {
        return "false";
    }
    return "";
}
function validateSelect(page, classs)
{
    var val = selectVal(page, classs);
    if (val == undefined || val <= 0) {
        return false;
    }
    return true;
}
function validateEmail(element)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(element.val());
}
/* Form - END */
function getUserImage(image)
{
    var gender = getValueFromSession('user_gender');
    var filename = 'images/profile.jpg';
    if (gender == 'male') {
        filename = 'images/male.jpg';
    }
    if (gender == 'female') {
        filename = 'images/female.jpg';
    }
    if (image && image.length > 10) {
        filename = BASE_PATH + 'uploads/images/' + image;
    }
    return filename;
}

function getUserImageGender(image, gender)
{
    var filename = 'images/profile.jpg';
    if (gender == 'male') {
        filename = 'images/male.jpg';
    }
    if (gender == 'female') {
        filename = 'images/female.jpg';
    }
    if (image && image.length > 10) {
        filename = BASE_PATH + 'uploads/images/' + image;
    }
    return filename;
}
//-------------------------------------------------
// ACCORDION CLASSES
//-------------------------------------------------
function registerAccordion(selector)
{
    $(selector + ' li a').off('click');
    var allListNavPanels = $(selector + ' li div');
    $(selector + ' li a').click(function (e)
    {
        killEvent(e);
        $this = $(this);
        $target = $this.next();
        if (!$target.hasClass('active')) {
            allListNavPanels.removeClass('active').slideUp(200);
            $target.addClass('active').slideDown(400);
        } else {
            allListNavPanels.removeClass('active').slideUp(200);
        }
        return false;
    });
}
//-------------------------------------------------
// QR SCANNER
//-------------------------------------------------
function onQRCodeStart(callback, feedback)
{
    // https://www.the-qrcode-generator.com/
    cordova.plugins.barcodeScanner.scan(
        function (result)
        {
            if (!result.cancelled) {
                //var url = result.text;
                // var url = decodeURIComponent(result.text);
                if (typeof callback == 'function') {
                    callback(result, feedback);
                    return false;
                }
            }
            if (!feedback) {
                showPopup('Oops', 'Invalid QR Code scanned. Please try again.');
            } else {
                setAlert(feedback, "warning", "Oops", "I do not recognize this the QR. Please try again.");
            }
            // alert("We got a barcode\n" + "Result: " + result.text + "\n" + "Format: " + result.format + "\n" + "Cancelled: " + result.cancelled);
        },
        function (error)
        {
            showPopup('Oops', 'Something went wrong. Please try again.')
        }
    );
}
//-------------------------------------------------
// CAMERA CAPTURE
//-------------------------------------------------
var _photoCaptured = false;
var _photoCapturedCallback;
function onCameraCaptureStart(callback)
{
    logger('onCameraCaptureStart');
    _photoCaptured = false;
    _photoCapturedCallback = callback;
    // https://github.com/phonegap-build/cordova-plugin-media-capture
    //navigator.device.capture.captureImage(onCameraCaptureSuccess, onCameraCaptureError, {limit: 1});
    // https://github.com/apache/cordova-plugin-camera
    navigator.camera.getPicture(onCameraCaptureSuccess, onCameraCaptureError, {
        quality: 70,
        allowEdit: true,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
    });
}
function onCameraCaptureSuccess(files)
{
    logger('onCameraCaptureSuccess');
    logger('onCameraCaptureSuccess ' + files);
    //var i, len, photo;
    //_photoCaptured = true;
    //for (i = 0, len = files.length; i < len; i += 1) {
    //    photo = files[i];
    //}
    if (typeof _photoCapturedCallback == 'function') {
        _photoCapturedCallback(files);
    }
}
function onCameraCaptureError(error)
{
    _photoCaptured = false;
}
//-------------------------------------------------
// CAMERA IMAGE UPLOAD
//-------------------------------------------------
var xmlhttp;
var photoTimeoutId;
var photosToUpload = [];
var photosCallback;
var photoUploadIndex = -1;
var photosUploaded = [];
var photosUploadIsComplete = true;
var photoUploadFailedCounter = 0;
function initResizeUploadPhotos(photos, callback)
{
    if (!photosUploadIsComplete) {
        //alert("we are still busy uploading images");
        return;
    }
    photosUploaded = [];
    photoUploadIndex = -1;
    photosToUpload = photos;
    photosCallback = callback;
    photoUploadFailedCounter = 0;
    photosUploadIsComplete = false;
    showLoader();
    setTimeout(onPhotoUploadProcessQueue, 200);
}
/**
 * Process thwe photo list
 * @returns {boolean}
 */
function onPhotoUploadProcessQueue()
{
    //logger('onPhotoUploadProcessQueue');
    photoUploadIndex++;
    // if we are at the end, complete
    if (photoUploadIndex >= photosToUpload.length) {
        onResizePhotosUploadedComplete()
        return true;
    }
    // check if the path is empty
    var path = photosToUpload[photoUploadIndex].path;
    if (!(path && path.length > 5)) {
        onResizePhotosUploadedComplete();
        return true;
    }
    resizePhotoBeforeUpload();
}
/**
 * Resize photo in <canvas>
 */
function resizePhotoBeforeUpload()
{
    //logger('resizePhotoBeforeUpload');
    //setLoadingText('Resizing Image');
    var img = new Image();
    img.onload = function ()
    {
        // resize on ratio
        var size = getImageSizeByAuto(this.width, this.height, 500, 500);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        canvas.width = size.width;
        canvas.height = size.height;
        ctx.drawImage(img, 0, 0, size.width, size.height);
        var dataUrl = canvas.toDataURL('image/jpeg', 0.7);
        document.getElementById("canvas-image-resize").src = dataUrl;
        //uploadPhotoToServerAjax();
        uploadPhotoToServerViaHttp();
    }
    // delay the resize, the screenshot freezes the app, we wait for the loader to show
    setTimeout(function ()
    {
        img.src = photosToUpload[photoUploadIndex].path;
    }, 200)
}
/**
 * Upload photo to server via XML Http Request
 */
function uploadPhotoToServerViaHttp()
{
    //logger('uploadPhotoToServerViaHttp');
    //setLoadingText('Uploading Image');
    photoTimeoutId = setTimeout(photoUploadTimeout, 20000);
    var formData = new FormData();
    formData.append("user_id", USER_ID);
    formData.append("session_token", SESSION_TOKEN);
    formData.append("title", photosToUpload[photoUploadIndex].title);
    formData.append("path", photosToUpload[photoUploadIndex].path);
    formData.append("source", $("#canvas-image-resize").attr('src'));
    xmlhttp = new XMLHttpRequest();
    xmlhttp.timeout = 25000;
    xmlhttp.ontimeout = photoUploadTimeout;
    xmlhttp.onprogress = onPhotoUploadProgress;
    xmlhttp.upload.onprogress = onPhotoUploadProgress;
    xmlhttp.onreadystatechange = onPhotoUploadStateChange;
    xmlhttp.open("POST", BASE_PATH + 'api/user/photo/transfer', true);
    xmlhttp.send(formData);
}
/**
 * Photo Upload State Change
 */
function onPhotoUploadStateChange()
{
    //logger('onPhotoUploadStateChange - ' + xmlhttp.status);
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        hideLoader();
        clearTimeout(photoTimeoutId);
        //logger('state - ' + xmlhttp.responseText);
        var sections = xmlhttp.responseText.split('|#|');
        // filename and the path of the image on the device
        var o = {
            'name': xmlhttp.responseText,
            'path': photosToUpload[photoUploadIndex].path
        };
        if (sections.length >= 2) {
            o = {'name': sections[0], 'path': sections[1]};
        }
        onPhotoUploadComplete(o);
    }
}
/**
 * Phto Upload Progress
 * @param e
 */
function onPhotoUploadProgress(e)
{
    if (e.lengthComputable) {
        var perc = Math.round(100 * (e.loaded / e.total)) + '%';
        //setLoadingText('Image ' + perc);
    }
}
/**
 * Photo Upload to server error / timeout
 */
function photoUploadTimeout()
{
    xmlhttp.abort();
    clearTimeout(photoTimeoutId);
    //alert("we are aborting: " + photoUploadIndex + ' - error index ' + photoUploadFailedCounter);
    // check network
    if (!isNetworkConnected()) {
        //onSyncError();
        return false;
    }
    photoUploadFailedCounter++;
    if (photoUploadFailedCounter > 3) {
        photoUploadFailedCounter = 0;
        onPhotoUploadComplete({
            'name': 'timeout',
            'path': photosToUpload[photoUploadIndex].path
        });
    }
    else {
        resizePhotoBeforeUpload();
    }
}
/**
 * Photo upload to server complete
 * @param o
 */
function onPhotoUploadComplete(o)
{
    //alert("onPhotoUploadComplete: " + o.name + ' -- ' + o.path);
    if (!(o == undefined || o == false)) {
        photosUploaded.push(o);
    }
    onPhotoUploadProcessQueue();
}
/**
 * When all the images has been uploaded
 * @returns {boolean}
 */
function onResizePhotosUploadedComplete()
{
    photosUploadIsComplete = true;
    if (typeof photosCallback == 'function') {
        photosCallback(photosUploaded);
        return false;
    }
    // for some reason the http did not abort / timeout
    photosCallback = null;
}
function uploadPhotoToServerAjax()
{
    setLoadingText('Uploading Image');
    var obj = {
        'user_id': USER_ID,
        'device_token': DEVICE_TOKEN,
        'title': photosToUpload[photoUploadIndex].title,
        'path': photosToUpload[photoUploadIndex].path,
        'source': $("#canvas-image-resize").attr('src')
    };
    $.ajax({
        type: "POST",
        url: BASE_PATH + 'ajax/' + UPLOAD_IMAGE_URL + "-ajax",
        data: obj,
        success: function (xhr)
        {
            alert("response");
            alert(xhr);
            //var result = xhr.responseText;
        },
        error: function ()
        {
            alert("error");
        }
    });
}
//-------------------------------------------------
// GEOLOCATION
//-------------------------------------------------
var _geolocationIndex = 0;
var _geolocation = {'latitude': 0, 'longitude': 0, 'accuracy': 0};
var _geolocationCallback;
function initGeolocation(callback)
{
    _geolocationIndex = 0;
    _geolocationCallback = callback;
    updateGeolocation();
}
function updateGeolocation()
{
    if (!navigator.geolocation) {
        return false;
    }
    if (_geolocationIndex >= 3) {
        if (typeof _geolocationCallback == 'function') {
            _geolocationCallback();
        }
        return true;
    }
    _geolocationIndex++;
    navigator.geolocation.getCurrentPosition(function (position)
    {
        _geolocation['latitude'] = position.coords.latitude;
        _geolocation['longitude'] = position.coords.longitude;
        _geolocation['accuracy'] = position.coords.accuracy;
        _geolocation['altitude_accuracy'] = position.coords.altitudeAccuracy;
        _geolocation['heading'] = position.coords.heading;
        _geolocation['speed'] = position.coords.speed;
        _geolocation['timestamp'] = position.timestamp;
        updateGeolocation();
    }, function (error)
    {
        updateGeolocation();
        if (!navigator.notification) {
            return false;
        }
        // navigator.notification.alert('Please turn on your GPS settings', function(){}, 'Oops', 'OK');
        // alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
    }, {enableHighAccuracy: true, timeout: 30000});
}
function initFancyBox()
{
    $(".linker").fancybox({
        margin: 50,
        padding: 5
    });
}
function validateResponse(response, button)
{
    if (response.success == false || response.error == true) {
        if (button.attr('data-feedback').length > 2) {
            showButtonAlertError(button, response.msg);
        }
        return false;
    }
    return true;
}

function createListFromArray(arr, key)
{
    return arr.map(function (obj)
    {
        return obj[key];
    }).join(',');
}

function checkPluralWord(word, total)
{
    if (parseInt(total) > 1) {
        return pluralize(word);
    }
    return word;
}