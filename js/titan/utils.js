//-------------------------------------------------
// Update Date
//-------------------------------------------------

function updateDateTime()
{
    $('#date-time').html(moment().format('dddd, [<strong>]D MMMM YYYY[</strong>] H:mm:ss'));
    setTimeout(function ()
    {
        updateDateTime();
    }, 1000);
    return true;
}

function getCurrentDateTime(dateOnly)
{
    if (dateOnly === true) {
        return moment().format('YYYY-MM-DD');
    }

    return moment().format('YYYY-MM-DD H:mm:ss');
}

//-------------------------------------------------
// Menu Items
//-------------------------------------------------

function stopSubmitButton(element, event)
{
    killEvent(event);
    disableButton(element);
    //$("#" + element.attr('data-feedback')).css('display', "none");
}

function killEvent(e)
{
    if (e.preventDefault) {
        e.preventDefault();
    }
    if (e.stopPropagation) {
        e.stopPropagation();
    }
}

/**
 * Prevent double tapping
 * @param element
 */
function disableEnableButton(element)
{
    disableButton(element);

    enableButton(element);
}

function disableButton(element)
{
    $(element).prop('disabled', true);
}

function enableButton(element)
{
    // small delay before we enable again
    setTimeout(function ()
    {
        element.prop('disabled', false);
    }, 300);
}

function checkAuthGuard(message)
{
    if (USER_IS_LOGGED_IN == false || USER_ID <= 0) {
        message = message ? message : 'Unfortunately you have to be signed in.';
        showPopupConfirm('User Required', message, function (buttonIndex)
        {
            if (buttonIndex == 1) {
                doLogout();
            }
        }, ['LOGIN', 'CANCEL']);
        return false;
    }

    return true;
}

/**
 * Loop through all the checkboxes and return the selected ids
 * @param boxes
 * @returns {Array}
 */
function getCheckboxIds(boxes)
{
    var ids = [];
    boxes.each(function ()
    {
        if ($(this).is(':checked')) {
            ids.push($(this).attr('data-id'));
        }
    });

    return ids;
}

function showNotification(message, level, timeout)
{
    timeout = timeout ? timeout : 4000;
    $('#notification .notify-small')
        .removeClass('alert-success')
        .removeClass('alert-info')
        .removeClass('alert-error')
        .removeClass('alert-warning');

    var level = level ? level : 'success';
    $('#notification .notify-small').addClass('alert-' + level);

    $("#notification .message").html(message);

    //$('#notification').fadeIn(400);
    // set css
    $('#notification').css({
        'top': '25%',
        'display': 'block',
        'visibility': 'visible'
    });

    // show
    $('#notification').animate({
        'top': '30%',
        'opacity': '1',
    }, 400);

    // hide
    setTimeout(function ()
    {
        $('#notification').animate({
            'top': '35%',
            'opacity': '0',
        }, 300, function ()
        {
            // reset css
            $('#notification').css({
                'top': '25%',
                'display': 'none',
                'visibility': 'hidden'
            });
        });
    }, timeout);
}

function convertStringToSlug(text)
{
    return text.toString().toLowerCase().trim()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')             // Trim - from end of text
        .replace(/-$/, '');             // Remove last floating dash if exists
}

function slugImageName(value)
{
    return convertStringToSlug(value.toLowerCase());
}

/**
 * Send a native sms
 *
 * @param to
 * @param message
 */
function sendNativeSMS(to, message)
{
    if (!sms) {
        return false;
    }

    sms.send(to, message, 'INTENT', function ()
    {
        //alert('Message sent successfully');
    }, function (e)
    {
        //alert('Message Failed:' + e);
    });
}