function goToPrevUrl()
{
    if (IS_PAGE_BUSY_ANIMATING == true) {
        return;
    }

    var l = PAGE_QUEUE.length;
    var pageId = URL_LOGIN;
    if (l > 1) {
        pageId = PAGE_QUEUE[l - 2];
        PAGE_QUEUE.pop();
    }

    // if back to touch - go back to prev page
    if (pageId == PAGE_TOUCH_AND_WIN) {
        goToPrevUrl();
        return;
    }

    PREV_PAGE_ANIMATE = true;
    goToPage(pageId, ANIMATION_BACK_IN, ANIMATION_BACK_OUT);

    if (pageId == PAGE_DASHBOARD || pageId == PAGE_ARTICLES) {
        HEADER.toggleMenuBar('articles');
    }
}

function goToPageDelay(pageId, delay)
{
    if (IS_PAGE_WAITING_FOR_DELAY == true) {
        return;
    }

    // set to true, for the dealy to work properly
    IS_PAGE_WAITING_FOR_DELAY = true;

    delay = (typeof delay === "undefined") ? 750 : delay;

    setTimeout(function ()
    {
        IS_PAGE_WAITING_FOR_DELAY = false;
        goToPage(pageId, ANIMATION_IN, ANIMATION_OUT);
    }, delay);
}

/**
 * Go To a specific PAGE / URL
 *
 * @param pageId
 * @param inClass
 * @param outClass
 */
function goToPage(pageId, inClass, outClass)
{
    // if we are busy goint to page, waiting for delay, or already on that page
    if (IS_PAGE_BUSY_ANIMATING == true || IS_PAGE_WAITING_FOR_DELAY == true || PAGE_SELECTED == pageId) {
        return;
    }

    // prepand 'page-' if the id does not contain it already
    pageId = (pageId.indexOf('page-') < 0 ? 'page-' + pageId : pageId);

    // resets
    FOOTER.toggle('');
    $('#btn-footer-submit').attr('data-type', '');

    switch (pageId) {
        // auth
        case PAGE_LANDING:
            HEADER.toggleHeaderScroll('auth');
            break;
        case PAGE_LOGIN:
            resetLoginForm();
            break;
        case PAGE_FORGOT_PASSWORD:
            resetForgotPasswordForm();
            break;
        case PAGE_REGISTER:
            resetRegisterForm();
            break;

        case PAGE_DASHBOARD:
            PAGE_QUEUE = ['page-dashboard'];
            break;

        case PAGE_MY_INTERESTS:
            break;
        case PAGE_EDIT_INTERESTS:
            FOOTER.toggle('edit');
            $('#btn-footer-submit').attr('data-type', 'edit-interests');
            break;

        case PAGE_SHOW_ARTICLE:
            FOOTER.toggle('article');
            break;

        case PAGE_RATING:
            resetRatingForm();
            HEADER.showPageMenuBar('rating');
            FOOTER.toggle('back');
            break;

        case PAGE_ARTICLES:

            break;

        case PAGE_ARTICLE_COMMENTS:
            break;

        case PAGE_PASSPORT:
            PASSPORT.getLoyalties();
            HEADER.showPageMenuBar('my passport');
            break;

        case PAGE_VOUCHERS:
            VOUCHER.getVouchers();
            HEADER.showPageMenuBar('my vouchers');
            break;

        case PAGE_TOUCH_AND_WIN:
            FOOTER.toggle('back');
            HEADER.showPageMenuBar('touch & win');
            break;

        case PAGE_TERMS_AND_CONDITIONS:
            FOOTER.toggle('back');
            HEADER.showPageMenuBar('terms & conditions');
            break;
        case PAGE_LOYALTIES_INFORMATION:
            FOOTER.toggle('back');
            HEADER.showPageMenuBar('loyalty & voucher');
            break;

        case PAGE_SERVER_HTML:
            FOOTER.toggle('back');
            HEADER.showPageMenuBar('claim voucher');
            break;
    }

    HEADER.syncMenuAndPage(pageId);

    goToPageLibrary(pageId, inClass, outClass);
}

/**
 * Force redirect to the page
 * Add timeouts to 'wait' for the 'not busy' flag
 * @param page
 */
function forcePageRedirect(page)
{
    // incase we are busy animating
    goToPage(page);
    goToPageDelay(page);
    setTimeout(function ()
    {
        goToPageDelay(page);
    }, 500);
    setTimeout(function ()
    {
        goToPageDelay(page);
    }, 1100);
}

/**
 * If no interests and we have a user
 * Go to the edit interests page
 */
function checkNoInterestsUser()
{
    // if no interests and user
    // we need to 'save' interests first
    if (INTERESTS.userPublications.length <= 1 &&
        INTERESTS.userCategories.length <= 1 &&
        USER_ID >= 1) {

        forcePageRedirect(PAGE_EDIT_INTERESTS);
    }
}

function toggleLogo(type)
{
    // HEADER.toggleLogoBack(type);
    setTimeout(function ()
    {
        // HEADER.toggleLogoBack(type);
    }, 350);
}