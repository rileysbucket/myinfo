//-------------------------------------------------
// Page Default Navigation
//-------------------------------------------------
var ANIMATION_IN = "moveFromRight";
var ANIMATION_OUT = "moveToLeft";

var ANIMATION_BACK_IN = 'moveFromLeft';
var ANIMATION_BACK_OUT = 'moveToRight';

//-------------------------------------------------
// CONSTANTS
//-------------------------------------------------
var APP_VERSION = '0.1';

var USER_ID = 0;
var USER_POINTS = 0;
var SESSION_TOKEN = 0;

var DEVICE_READY = false;
var USER_SKIPPED = false;
var USER_IS_LOGGED_IN = false;

var LOCAL_DATA_SUFFIX = '_date';

var MOMENT_FORMAT_SHORT = 'YYYY-MM-DD';
var MOMENT_FORMAT_FULL = 'YYYY-MM-DD HH:mm:ss';

//-------------------------------------------------
// All the page ids / the urls we can go to
//-------------------------------------------------

var PAGE_LANDING = 'page-landing';
var PAGE_LOGIN = "page-login";
var PAGE_FORGOT_PASSWORD = "page-forgot-password";
var PAGE_REGISTER = "page-register";
var PAGE_FORGOT_PASSWORD = 'page-forgot-password';
var PAGE_DASHBOARD = "page-dashboard";
var PAGE_MY_INTERESTS = "page-my-interests";
var PAGE_EDIT_INTERESTS = "page-edit-interests";

var PAGE_ARTICLES = 'page-articles'; // where a publication or category articles will be
var PAGE_SHOW_ARTICLE = 'page-show-article';
var PAGE_ARTICLE_COMMENTS = 'page-article-comments';

var PAGE_RATING = 'page-rating';
var PAGE_PASSPORT = 'page-passport';
var PAGE_VOUCHERS = 'page-vouchers';
var PAGE_TOUCH_AND_WIN = 'page-touch-and-win';
var PAGE_TERMS_AND_CONDITIONS = 'page-terms-and-conditions';
var PAGE_LOYALTIES_INFORMATION = 'page-loyalties-information';

// feedback pages - do not allow the back button to work
var PAGE_FEEDBACK = "page-feedback";
var PAGE_SERVER_HTML = "page-server-html";


//-------------------------------------------------
// Server Method
//-------------------------------------------------
var BASE_PATH = "https://www.my.na/app/";
var IMAGES_PATH = 'https://cdn.my.na/assets/images/';
var ARTICLE_IMAGES_PATH = 'https://cdn.my.na/my_images/set/800/533/90/?src=assets/images/';

var URL_LOGIN = 'login';
var URL_REGISTER = 'register';
var URL_FORGOT_PASSWORD = 'forgot-password';
var URL_INTERESTS = 'interests';
var URL_UPDATE_INTERESTS = 'update_interests';
var URL_ARTICLE_COMMENTS = 'category_content';
var URL_ARTICLE_COMMENTS_CREATE = '';

var URL_LOCATION = 'profile/location';
var URL_RATING = 'submit_rating';

var URL_USER_PASS = 'user_code';
var URL_LOYALTIES = 'current_promotions';
var URL_VOUCHERS = 'current_vouchers';
var URL_CLAIM_VOUCHER = 'claim_voucher';
var URL_ADVERTS = 'adverts';

var URL_SOCIAL_MEDIA = "na_content";
var URL_ARTICLES_PUBLICATION = "publication_content";
var URL_ARTICLES_CATEGORIES = "category_content";

//-------------------------------------------------
// CONSTANTS INITIALIZED
//-------------------------------------------------
var SCROLLER;
var HEADER;
var FOOTER;
var INTERESTS;
var DASHBOARD;
var COMMENTS;
var SEARCH;
var PASSPORT;
var VOUCHER;
var TOUCH_AND_WIN;
var ADVERTS;
var WALKTHROUGH;