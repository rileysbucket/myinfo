/** LOCALHOST / DEVELOPMENT */
// $(function ()
// {
//     onDeviceReady();
// });

/** ONLINE - APP */
DEBUG = false;
IS_DESKTOP = false;
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady()
{
    if (DEBUG) {
        // $('#logger').show();
        $('#btn-logger-hide').click(function ()
        {
            $('#logger').animate({height: 0}, 500, function ()
            {
                $('#logger').hide();
            });
        });
    }

    //return false;
    //doLogout();
    initLibrary();

    SCROLLER = new ScrollClass();
    HEADER = new HeaderClass();
    FOOTER = new FooterClass();
    INTERESTS = new InterestsClass();
    COMMENTS = new CommentsClass();
    SEARCH = new SearchClass();
    VOUCHER = new VoucherClass();
    TOUCH_AND_WIN = new TouchAndWinClass();
    PASSPORT = new MyPassportClass();
    ADVERTS = new AdvertsClass();
    WALKTHROUGH = new WalkthroughClass();

    // register events (tap)
    registerAuth();
    registerQrCode();
    registerRating();

    // dashboard is needed before login
    DASHBOARD = new ArticlesPage();

    // start / init app
    updateLoginStatus();

    // deeplink into a page - development
    if (USER_IS_LOGGED_IN && DEBUG) {
        //saveDeviceLocation(); // track location
        //setTimeout(function ()
        //{
        //goToPageDelay(PAGE_DASHBOARD);
        //goToPageDelay(PAGE_PASSPORT);
        //goToPageDelay(PAGE_VOUCHERS);
        //goToPageDelay(PAGE_ARTICLE_COMMENTS);

        //goToPageDelay(PAGE_RATING);
        //goToPageDelay(PAGE_MY_INTERESTS);
        //goToPageDelay(PAGE_EDIT_INTERESTS);

        //DASHBOARD.tmpSelectedArticle();

        //TOUCH_AND_WIN.reset();
        //goToPageDelay(PAGE_TOUCH_AND_WIN);
        //}, 1800);

        // save app version - user is using
        //updateAppVersion();
    }

    // WALKTHROUGH.initAndShow();
}

/**
 * Add User to session
 */
function updateUser(data)
{
    USER_ID = data['id'];
    SESSION_TOKEN = data['session_token'];

    addKeyValueToSession("myinfo_user_id", data['id']);
    addKeyValueToSession("myinfo_user_fullname", data['u_name']);

    addKeyValueToSession("user_image", data['img_file']);
    addKeyValueToSession("user_facebook_id", data['fb_id']);
    addKeyValueToSession("user_points", data['points']);
    addKeyValueToSession("session_token", data['session_token']);
    addKeyValueToSession("user_gender", data['gender'] ? data['gender'] : 'male');
    addKeyValueToSession("user_born_at", data['born_at']);

    // set login status / update user js vars and html placeholders
    updateLoginStatus();
}

/**
 * Update the status of logged in or not
 */
function updateLoginStatus()
{
    USER_ID = getValueFromSession('myinfo_user_id');
    USER_POINTS = getValueFromSession('user_points');
    SESSION_TOKEN = getValueFromSession('session_token');
    USER_IS_LOGGED_IN = (isEmpty(USER_ID) == false && USER_ID > 0) ? true : false;

    // set logged in status
    addKeyValueToSession("logged_in", USER_IS_LOGGED_IN);

    // startup data
    HEADER.init();
    SEARCH.init();

    initDateOfBirth();
    ADVERTS.resetAdverts();
    if (USER_IS_LOGGED_IN) {
        updateLoginInformation();

        INTERESTS.init();
        VOUCHER.getVouchers();
        PASSPORT.getUserPassInfo();
        PASSPORT.getLoyalties();

        // only activate when logged in
        $('.authenticated-hidden').hide();
        $('.authenticated-visible').show();
    } else {
        $('.authenticated-hidden').show();
        $('.authenticated-visible').hide();
    }

    SCROLLER.registerEvents();

    hideLoader();
}

/**
 * Update the users information to html placeholders
 */
function updateLoginInformation()
{
    // set the logged in user name
    $('.user-fullname').html(getValueFromSession('myinfo_user_fullname'));
    $('.user-points').html(getValueFromSession('user_points', '0'));
    $('.user-gender').html(getValueFromSession('user_gender').ucFirst());

    $('.btn-gender').removeClass('active');

    // if male / female
    if ($('.btn-gender.' + getValueFromSession('user_gender'))) {
        $('.btn-gender.' + getValueFromSession('user_gender')).addClass('active');
    }

    if (getValueFromSession('user_born_at').length > 4) {
        $('.user-born').html(moment(getValueFromSession('user_born_at'), MOMENT_FORMAT_SHORT).format('DD MMMM YYYY'));
        $('.in-born').val(moment(getValueFromSession('user_born_at'), MOMENT_FORMAT_SHORT).format('YYYY-MM-DD'));
    }
}

function doLogout()
{
    // clear local storage and redirect to home page
    validateSession(true, true);
}