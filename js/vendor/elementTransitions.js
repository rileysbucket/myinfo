/* 
    Element Transition
    - Also remove the <a> click, manual toPage
*/

var PageTransitions = (function($) {
    animEndEventNames = {
        'WebkitAnimation': 'webkitAnimationEnd',
        'OAnimation': 'oAnimationEnd',
        'msAnimation': 'MSAnimationEnd',
        'animation': 'animationend'
    }

    function getTransitionPrefix() {
        var b = document.body || document.documentElement;
        var s = b.style;
        var p = 'animation';
        if(typeof s[p] == 'string') { return 'animation'; }

        // Tests for vendor specific prop
        v = ['Moz', 'Webkit', 'Khtml', 'O', 'ms'],
        p = p.charAt(0).toUpperCase() + p.substr(1);
        for( var i=0; i<v.length; i++ ) {
            if(typeof s[v[i] + p] == 'string')
                return v[i] + p;
        }
        return false;
    }

    // animation end event name
    animEndEventName = animEndEventNames[getTransitionPrefix()];

    function init() {
        $(".et-page").each(function() {
            $(this).data('originalClassList', $(this).attr('class'));
        });

        $(".et-wrapper").each(function() {
            $(this).data('current', 0);
            $(this).data('isAnimating', "false");
            $(this).children(".et-page").first().addClass('et-page-current');
        });
    }

    function animateToPage(wrapper, toPage, inClass, outClass, callback) {
        var block = (wrapper);
        var inClass = formatClass(inClass);
        var outClass = formatClass(outClass);
        var pages = block.children('.et-page');

        // current index
        var toPageIndex = 0;
        var fromPage = pages[0];
        var fromPageIndex = block.data('current');

        // find the current page and to page index
        for (var i = 0; i < pages.length; i++) {
            // current page
            if(i == fromPageIndex) {
                fromPage = $(pages[i]);
            }
            // new page index
            if(toPage.attr('id') == $(pages[i]).attr('id')) {
                toPageIndex = i;
            }
        };

        // dont animate to same page
        if(fromPageIndex == toPageIndex) { return false; }

        // is busy animating
        if(block.data('isAnimating') == "true") { return false; }

        block.data('current', toPageIndex);
        block.data('isAnimating', "true");

        // add class to new page
        fromPage.addClass(outClass);
        toPage.addClass(inClass);
        toPage.addClass('et-page-current');

        // a quick fix for mobile - 'reset' vars after 1 sec
        setTimeout(function() {
            // reset classes / animation vars
            onEndAnimation(fromPage, toPage, block);

            // if callback
            if(typeof callback == 'function' ) { 
                callback(block, toPage, fromPage);
            }
        }, 1000);
    }

    function onEndAnimation($outpage, $inpage, block) {
        resetPage($outpage, $inpage);
        block.data('isAnimating', false);
    }

    function resetPage($outpage, $inpage) {
        $outpage.attr('class', $outpage.data('originalClassList'));
        $inpage.attr('class', $inpage.data('originalClassList') + ' et-page-current');
    }

    function formatClass(str) {
        classes = str.split(" ");
        output = "";
        for(var n=0; n<classes.length; n++){
            output += " pt-page-" + classes[n];
        }
        return output;
    }
    return {
        init : init,
        animateToPage: animateToPage
    };
})($);